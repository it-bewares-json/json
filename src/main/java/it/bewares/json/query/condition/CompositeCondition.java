/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.query.condition;

import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

/**
 * A {@code CompositeCondition} is a statemen, for a given JSON value based on
 * two other {@code Condition}s.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
public abstract class CompositeCondition extends Condition {

    @Getter
    @Setter
    private Condition conditionA;

    @Getter
    @Setter
    private Condition conditionB;

    /**
     * Allocates a {@code CompositeCondition} as a composition of the conditions
     * {@code conditionA} and {@code conditionB}.
     *
     * @param conditionA the first condition
     * @param conditionB the second condition
     */
    public CompositeCondition(Condition conditionA, Condition conditionB) {
        this.conditionA = conditionA;
        this.conditionB = conditionB;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object b) {
        if (!(b instanceof CompositeCondition)) {
            return false;
        }

        if (!this.conditionA.equals(((CompositeCondition) b).conditionA)) {
            return false;
        }

        if (!this.conditionB.equals(((CompositeCondition) b).conditionB)) {
            return false;
        }

        return super.equals(b);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int hash = super.hashCode() + 5;
        hash = 23 * hash + Objects.hashCode(this.conditionA);
        hash = 23 * hash + Objects.hashCode(this.conditionB);
        return hash;
    }

}
