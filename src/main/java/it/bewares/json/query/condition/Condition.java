/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.query.condition;

import it.bewares.json.tree.JSONValue;

/**
 * A {@code Condition} is a statement, either true or or false, for a given
 * {@code JSONValue}, its absolute path and respective key (or index).
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
public abstract class Condition {

    /**
     * Determines if this condition applies to the specified JSON value and its
     * absolute path and respective key (or index).
     *
     * @param relative the specified JSON value which shall be checked
     * @param absolute the absolute JSON value
     * @param key the respective key (or index)
     * @return if this condition applies to the specified JSON value
     */
    public abstract boolean applies(JSONValue relative, JSONValue absolute, JSONValue key);

    /**
     * Compares this object with another Object to check if both are
     * {@code Condition}s and if their important values are equal.
     *
     * @param b Object to be checked against
     * @return a {@code boolean}, {@code true} if o is a {@code JSONString} and
     * both wrapped values are equal, otherwise {@code false}
     */
    @Override
    public boolean equals(Object b) {
        return b instanceof Condition;
    }

    /**
     * Calculates a hash code value for this {@code Condition} based on its
     * important values' hash code.
     *
     * @return a hash code value for this {@code JSONString}
     */
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

}
