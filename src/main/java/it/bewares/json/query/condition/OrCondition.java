/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.query.condition;

import it.bewares.json.tree.JSONValue;

/**
 * A {@code OrCondition} is a {@code CompositeCondition} based on two other
 * {@code Condition}s, where both are composed using the logical (inclusive) OR
 * as the logical connective.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
public class OrCondition extends CompositeCondition {

    /**
     * Allocates a {@code OrCondition} as a composition of the conditions
     * {@code conditionA} and {@code conditionB}.
     *
     * @param conditionA the first condition
     * @param conditionB the second condition
     */
    public OrCondition(Condition conditionA, Condition conditionB) {
        super(conditionA, conditionB);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean applies(JSONValue relative, JSONValue absolute, JSONValue key) {
        return this.getConditionA().applies(relative, absolute, key)
                || this.getConditionB().applies(relative, absolute, key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object b) {
        if (!(b instanceof OrCondition)) {
            return false;
        }

        return super.equals(b);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int hash = super.hashCode() + 3;
        return hash;
    }

}
