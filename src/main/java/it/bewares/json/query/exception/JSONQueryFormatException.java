/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.query.exception;

/**
 * Thrown to indicate that the application has attempted to create a
 * {@code JSONQuery}, but that the String does not have the appropriate format.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
public class JSONQueryFormatException extends IllegalArgumentException {

    private static final long serialVersionUID = -5437119068300148411L;

    /**
     * Allocates a {@code JSONQueryFormatException} with no detail message.
     */
    public JSONQueryFormatException() {
        super();
    }

    /**
     * Allocates a {@code JSONQueryFormatException} with the specified detail
     * message.
     *
     * @param message the detail message
     */
    public JSONQueryFormatException(String message) {
        super(message);
    }

    /**
     * Allocates a {@code JSONQueryFormatException} with the specified detail
     * message and cause.
     *
     * @param message the detail message
     * @param cause the cause for this exception
     */
    public JSONQueryFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Allocates a {@code JSONQueryFormatException} with the specified cause.
     *
     * @param cause the cause for this exception
     */
    public JSONQueryFormatException(Throwable cause) {
        super(cause);
    }

}
