/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.query;

import it.bewares.json.tree.JSONArray;
import it.bewares.json.tree.JSONValue;
import it.bewares.json.tree.JSONObject;
import java.util.regex.Pattern;
import lombok.Getter;
import it.bewares.json.query.exception.JSONQueryFormatException;
import it.bewares.json.query.exception.JSONQueryResolveException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;

/**
 * A {@code JSONPointer} is a wrapper for a JSON Pointer in String
 * representation.
 *
 * JSON Pointer refers to the JavaScript Object Notation Pointer in its
 * definition by the IETF in RFC 6901.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc6901">RFC 6901</a>
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
public class JSONPointer implements JSONQuery {

    private static String POINTER_NODE_REGEX = "\\/((?:[^~\\/\\n]|~[0-1])*)";
    private static String POINTER_REGEX = "^(?:" + POINTER_NODE_REGEX + ")*$";

    @Getter
    private String pointer;

    @Getter
    private String[] path;

    /**
     * Allocates a {@code JSONPointer} pointing by default to the respective
     * {@code JSONStructure} itself.
     */
    public JSONPointer() {
        this("");
    }

    /**
     * Allocates a {@code JSONPointer} with a specified Pointer String.
     *
     * @param pointer String representing the Pointer
     * @throws JSONQueryFormatException if the specified Pointer is invalid
     */
    public JSONPointer(String pointer) throws JSONQueryFormatException {
        this.setPointer(pointer);
    }

    /**
     * Allocates a {@code JSONPointer} with a specified path representing the
     * Pointer.
     *
     * @param path String array representing the Pointer
     * @throws JSONQueryFormatException if the specified Pointer is invalid
     */
    public JSONPointer(String[] path) throws JSONQueryFormatException {
        String pointer = "";
        for (String pathNode : path) {
            pointer = pointer.concat("/").concat(pathNode.replace("~", "~0").replace("/", "~1"));
        }

        this.path = path;
        this.pointer = pointer;
    }

    /**
     * Sets the Pointer to the specified Pointer String.
     *
     * @param pointer the String which shall be the new Pointer
     * @throws JSONQueryFormatException if the specified Pointer String is not
     * valid
     */
    public void setPointer(String pointer) throws JSONQueryFormatException {
        if (!JSONPointer.isValid(pointer)) {
            throw new JSONQueryFormatException();
        }

        this.pointer = pointer;

        Matcher pathMatch = Pattern.compile(JSONPointer.POINTER_NODE_REGEX).matcher(pointer);
        List<String> path = new ArrayList<>();
        while (pathMatch.find()) {
            path.add(pathMatch.group(1));
        }

        this.path = path.toArray(new String[path.size()]);
    }

    /**
     * Determines if the specified String is a valid Pointer.
     *
     * @param pointer String which shall represent a valid Pointer
     * @return {@code true} if the specified String is a valid Pointer,
     * otherwise {@code false}
     */
    public static boolean isValid(String pointer) {
        return pointer.matches(JSONPointer.POINTER_REGEX);
    }

    /**
     * Creates a new Pointer pointing at the Pointer's parent value.
     *
     * @return the Pointer pointing at the Pointer's parent value
     */
    public JSONPointer getParentPointer() {
        String[] path = Arrays.copyOf(this.path, this.path.length - 1);
        return new JSONPointer(path);
    }

    /**
     * Determines the {@code JSONValue} at which the {@code JSONPointer} points
     * at.
     *
     * @param input value at which is Pointer
     * @return the {@code JSONValue} at which is pointed by the specified
     * Pointer
     */
    @Override
    public JSONValue find(JSONValue input) {
        JSONValue json = input;

        try {
            for (String pathNode : path) {
                if (json instanceof JSONObject) {
                    JSONObject jsonObject = (JSONObject) json;

                    if (jsonObject.containsKey(pathNode)) {
                        json = jsonObject.get(pathNode);
                    } else {
                        throw new NullPointerException();
                    }
                } else if (json instanceof JSONArray) {
                    json = ((JSONArray) json).get(Integer.parseInt(pathNode));
                } else {
                    throw new NullPointerException();
                }
            }
        } catch (IndexOutOfBoundsException | NullPointerException ex) {
            throw new JSONQueryResolveException("No value could be found.", ex);
        }

        return json;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<JSONValue> resolve(JSONValue input) {
        return new ArrayList<>(Arrays.asList(this.find(input)));
    }

    /**
     * Compares this object with another Object to check if both are
     * {@code JSONPointer}s and if their {@code path}s are equal.
     *
     * @param b Object to be checked against
     * @return a {@code boolean}, {@code true} if o is a {@code JSONPointer} and
     * both paths are equal, otherwise {@code false}
     */
    @Override
    public boolean equals(Object b) {
        if (b instanceof JSONPointer) {
            return this.getPointer().equals(((JSONPointer) b).getPointer());
        }
        return false;
    }

    /**
     * Calculates a hash code value for this {@code JSONPointer} based on its
     * path hash code.
     *
     * @return a hash code value for this {@code JSONPointer}
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Arrays.deepHashCode(this.path);
        return hash;
    }

    /**
     * Returns a string representing this {@code JSONPointer}.
     *
     * @return a string representing this pointer
     */
    @Override
    public String toString() {
        return this.getPointer();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
