/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.query;

import it.bewares.json.query.exception.JSONQueryResolveException;
import it.bewares.json.tree.JSONValue;
import java.util.Collection;

/**
 * All implementations of this Interface shall select JSON values based on a
 * selector.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
public interface JSONQuery extends Cloneable {

    /**
     * Determines an arbitrary {@code JSONValue} which fulfill the query's
     * conditions.
     *
     * @param input value on which the query shall be apply
     * @return the {@code JSONValue} which fulfill the query's conditions
     */
    default public JSONValue find(JSONValue input) {
        Collection<JSONValue> results = this.resolve(input);

        if (results == null || results.isEmpty()) {
            throw new JSONQueryResolveException("no value found");
        }

        return results.iterator().next();
    }

    /**
     * Determines all {@code JSONValue}s which fulfill the query's conditions.
     *
     * @param input value on which the query shall be apply
     * @return the {@code JSONValue} which fulfill the query's conditions
     */
    public Collection<JSONValue> resolve(JSONValue input);

}
