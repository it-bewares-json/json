/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.query;

import it.bewares.json.JSONFactory;
import it.bewares.json.io.exception.JSONFormatException;
import it.bewares.json.io.generator.SimpleJSONGenerator;
import it.bewares.json.io.parser.JSONStreamParser;
import it.bewares.json.query.condition.AndCondition;
import it.bewares.json.query.condition.BasicCondition;
import it.bewares.json.query.condition.Condition;
import it.bewares.json.query.condition.OrCondition;
import it.bewares.json.query.exception.JSONQueryFormatException;
import it.bewares.json.query.exception.JSONQueryResolveException;
import it.bewares.json.tree.JSONArray;
import it.bewares.json.tree.JSONObject;
import it.bewares.json.tree.JSONString;
import it.bewares.json.tree.JSONValue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * A {@code JSONSelector} is a wrapper for a JSON Selector in String
 * representation.
 *
 * JSON Selector refers to the JavaScript Object Notation Selector in its
 * definition by Markus Goellnitz.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 * @see <a href="http://bewares.it/json/selector">JSON Selector</a>
 */
@Slf4j
public class JSONSelector implements JSONQuery {

    private static String WS_REGEXP = "[ \\t\\n\\r]*";
    private static String NUMBER_REGEXP = "-?(?:0|[1-9]\\d*)(?:\\.\\d+)?(?:[eE](?:\\+|\\-)?\\d+)?";
    private static String STRING_REGEXP = "\\\"(?:[^\\\"\\\\\u0000-\u001F]|\\\\(?:[\\\"\\\\\\/bfnrt]|u[\\dA-Fa-f]{4}))*\\\"";
    private static String INT_REGEXP = "0|[1-9]\\d*";
    private static String EXP_REGEXP = "\\((?:[^\\)\\\"]|\\\"[^\\\"]*\\\")+\\)";
    private static String NODE_SELECTOR_REGEXP = "(?:" + WS_REGEXP + "\\." + WS_REGEXP + ")((?:" + STRING_REGEXP + ")|(?:" + INT_REGEXP + ")|(?:" + EXP_REGEXP + "))";
    private static String SELECTOR_REGEXP = "^(?:" + NODE_SELECTOR_REGEXP + ")*" + WS_REGEXP + "$";
    private static String DEFINITE_SELECTOR_REGEXP = "^(?:@(?:key|index)?|#)(?:" + WS_REGEXP + "\\." + WS_REGEXP + "((?:" + STRING_REGEXP + ")|(?:" + INT_REGEXP + ")))*" + WS_REGEXP + "(?:\\.length\\(\\))?";

    private static String firstValue(String input) {
        LOG.debug("firstValue({}) init", input);

        if (input.length() == 0) {
            return null;
        }

        int length = 1;

        switch (input.charAt(0)) {
            case JSONFactory.STRING_SURROUND:
                Matcher stringPatternMatcher = Pattern.compile("^(" + JSONSelector.STRING_REGEXP + ")").matcher(input);
                if (stringPatternMatcher.find()) {
                    length = stringPatternMatcher.end();
                } else {
                    throw new JSONQueryFormatException("Unfinished String");
                }
                break;

            case '#':
            case '@':
                Matcher selectorPatternMatcher = Pattern.compile(JSONSelector.DEFINITE_SELECTOR_REGEXP).matcher(input);
                if (selectorPatternMatcher.find()) {
                    length = selectorPatternMatcher.end();
                } else {
                    throw new JSONQueryFormatException("Unfinished Selector");
                }
                break;

            default:
                Matcher numberPatternMatcher = Pattern.compile("^(" + JSONSelector.NUMBER_REGEXP + ")").matcher(input);
                if (numberPatternMatcher.find()) {
                    length = numberPatternMatcher.end();
                } else if (Pattern.compile("^(true|null)").matcher(input).find()) {
                    length = 4;
                } else if (Pattern.compile("^false").matcher(input).find()) {
                    length = 5;
                } else {
                    throw new JSONQueryFormatException("\"" + input + "\" is not a valid input");
                }
                break;
        }

        return input.substring(0, length);
    }

    private static BasicCondition.Comparator comparatorMapping(String comparatorString) {
        switch (comparatorString) {
            case "<":
                return BasicCondition.Comparator.LESS;
            case ">":
                return BasicCondition.Comparator.GREATER;
            case "<=":
                return BasicCondition.Comparator.LESS_OR_EQUAL;
            case ">=":
                return BasicCondition.Comparator.GREATER_OR_EQUAL;
            case "==":
                return BasicCondition.Comparator.EQUAL;
            case "!=":
                return BasicCondition.Comparator.NOT_EQUAL;
            case "~":
                return BasicCondition.Comparator.MATCH;
        }

        throw new JSONQueryFormatException("\"" + comparatorString + "\" is not a valid comparator");
    }

    private Condition compileCondition(String conditionString) {
        conditionString = conditionString
                .replace("^" + JSONSelector.WS_REGEXP, "")
                .replace(JSONSelector.WS_REGEXP + "$", "");
        conditionString = conditionString.substring(1, conditionString.length() - 1);
        String connective = "";
        Condition condition = null;

        while (conditionString.length() > 0) {
            String a;
            try {
                conditionString = conditionString
                        .replaceAll("^" + JSONSelector.WS_REGEXP + "(?=[\\\"#@\\-0-9tfn])", "");
                a = JSONSelector.firstValue(conditionString);
                conditionString = conditionString.substring(a.length());
            } catch (NullPointerException ex) {
                throw new JSONQueryFormatException(ex);
            }

            String comparatorString;
            Matcher comparatorMatcher = Pattern.compile("^" + WS_REGEXP + "([<>!\\=]\\=)" + WS_REGEXP).matcher(conditionString);
            if (!comparatorMatcher.find()
                    && !(comparatorMatcher = Pattern.compile("^" + WS_REGEXP + "([<>~])" + WS_REGEXP).matcher(conditionString)).find()) {
                throw new JSONQueryFormatException("\"" + conditionString + "\" is not a valid expression");
            }
            comparatorString = comparatorMatcher.group(1);
            conditionString = conditionString.substring(comparatorMatcher.end());

            String b;
            try {
                conditionString = conditionString
                        .replaceAll("^" + JSONSelector.WS_REGEXP + "(?=[\\\"#@\\-0-9tfn])", "");
                b = JSONSelector.firstValue(conditionString);
                conditionString = conditionString.substring(b.length());
            } catch (NullPointerException ex) {
                throw new JSONQueryFormatException(ex);
            }

            Object aVal = a;
            Object bVal = b;
            try {
                if (!a.startsWith("@") && !a.startsWith("#")) {
                    aVal = JSON.parse(a);
                }
                if (!b.startsWith("@") && !b.startsWith("#")) {
                    bVal = JSON.parse(b);
                }
            } catch (JSONFormatException ex) {
                throw new JSONQueryFormatException(ex);
            }

            BasicCondition.Comparator comparator = JSONSelector.comparatorMapping(comparatorString);
            if (BasicCondition.Comparator.MATCH.equals(comparator)) {
                bVal = Pattern.compile(((JSONString) bVal).getValue());
            }

            Condition basicCondition = new BasicCondition(aVal, comparator, bVal);

            switch (connective) {
                case "&&":
                    condition = new AndCondition(condition, basicCondition);
                    break;
                case "||":
                    condition = new OrCondition(condition, basicCondition);
                    break;
                case "":
                    condition = basicCondition;
                    break;
                default:
                    throw new JSONQueryFormatException();
            }

            Matcher logicalConnectiveMatcher = Pattern.compile(JSONSelector.WS_REGEXP + "(\\|\\||&&)" + JSONSelector.WS_REGEXP).matcher(conditionString);
            if (logicalConnectiveMatcher.find()) {
                connective = logicalConnectiveMatcher.group(1);
                conditionString = conditionString.substring(logicalConnectiveMatcher.end());
            } else if (conditionString.replaceAll(JSONSelector.WS_REGEXP, "").length() > 0) {
                throw new JSONQueryFormatException("\"" + conditionString + "\" is not a valid expression");
            } else {
                break;
            }
        }

        return condition;
    }

    /**
     * Determines if the specified String is a valid Selector.
     *
     * @param selector String which shall represent a valid Selector
     * @return {@code true} if the specified String is a valid Selector,
     * otherwise {@code false}
     */
    public static boolean isValid(String selector) {
        return selector.matches(JSONSelector.SELECTOR_REGEXP);
    }

    @Inject
    private JSONFactory JSON;

    @Getter
    private String selector;

    private String[] selectors;

    @Getter
    private List<Condition> conditions;

    /**
     * Allocates a {@code JSONSelector} selecting by default the respective
     * {@code JSONStructure} itself.
     */
    public JSONSelector() {
        this("");
    }

    /**
     * Allocates a {@code JSONSelector} with a specified Selector String.
     *
     * @param selector String representing the Selector
     * @throws JSONQueryFormatException if the specified Selector String is
     * invalid
     */
    public JSONSelector(String selector) throws JSONQueryFormatException {
        this.JSON = new JSONFactory(JSONStreamParser.class, SimpleJSONGenerator.class);

        this.setSelector(selector);
    }

    /**
     * Sets the Selector to the specified Selector String.
     *
     * @param selector the String which shall be the new Selector
     * @throws JSONQueryFormatException if the specified Selector String is not
     * valid
     */
    public void setSelector(String selector) throws JSONQueryFormatException {
        if (!JSONSelector.isValid(selector)) {
            throw new JSONQueryFormatException();
        }

        this.selector = selector;

        Matcher pathMatch = Pattern.compile(JSONSelector.NODE_SELECTOR_REGEXP).matcher(selector);

        List<String> selectors = new ArrayList<>();
        this.conditions = new ArrayList<>();

        while (pathMatch.find()) {
            String path = pathMatch.group(1);
            selectors.add(path);

            if (path.startsWith("(")) {
                conditions.add(this.compileCondition(path));
            } else {
                conditions.add(null);
            }
        }

        this.selectors = selectors.toArray(new String[selectors.size()]);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<JSONValue> resolve(JSONValue absolute) {
        Collection<JSONValue> results = new JSONArray<>(absolute);

        try {
            for (int i = 0; i < selectors.length; i++) {
                String selectorNode = selectors[i];

                Collection<JSONValue> oldResults = new JSONArray<>(results);
                results.clear();

                for (JSONValue json : oldResults) {
                    if (json instanceof JSONObject) {
                        if (selectorNode.startsWith("(")) {
                            Condition condition = this.conditions.get(i);
                            for (String key : ((JSONObject<?>) json).keySet()) {
                                JSONValue node = ((JSONObject) json).get(key);

                                if (condition.applies(node, absolute, JSON.createValue(key))) {
                                    results.add(node);
                                }
                            }
                        } else {
                            results.add(((JSONObject) json).get((JSONString) JSON.parse(selectorNode)));
                        }
                    } else if (json instanceof JSONArray) {
                        if (selectorNode.startsWith("(")) {
                            Condition condition = this.conditions.get(i);
                            for (int index = 0; index < ((JSONArray) json).size(); index++) {
                                JSONValue node = ((JSONArray) json).get(index);

                                if (condition.applies(node, absolute, JSON.createValue(index))) {
                                    results.add(node);
                                }
                            }
                        } else {
                            results.add(((JSONArray) json).get(Integer.parseInt(selectorNode)));
                        }
                    } else {
                        throw new JSONQueryResolveException("Selector does not match value's tree.");
                    }
                }
            }
        } catch (IndexOutOfBoundsException | NullPointerException | IllegalArgumentException | ClassCastException ex) {
            throw new JSONQueryResolveException("No value could be found.", ex);
        }

        LOG.debug("findAll({}) found {} results for this={}", absolute.toString(), results.size(), this.selector);

        return results;
    }

    /**
     * Compares this object with another Object to check if both are
     * {@code JSONSelector}s and if their {@code selector}s and
     * {@code condition}s are equal.
     *
     * @param b Object to be checked against
     * @return a {@code boolean}, {@code true} if o is a {@code JSONSelector}
     * and both wrapped selector and conditions are equal, otherwise
     * {@code false}
     */
    @Override
    public boolean equals(Object b) {
        if (b instanceof JSONSelector) {
            return this.selectors.equals(((JSONSelector) b).selectors)
                    && this.conditions.equals(((JSONSelector) b).conditions);
        }
        return false;
    }

    /**
     * Calculates a hash code value for this {@code JSONSelector} based on its
     * path hash code.
     *
     * @return a hash code value for this {@code JSONSelector}
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Arrays.deepHashCode(this.selectors);
        hash = 67 * hash + Objects.hashCode(this.conditions);
        return hash;
    }

    /**
     * Returns a string representing this {@code JSONSelector}.
     *
     * @return a string representing this selector
     */
    @Override
    public String toString() {
        return this.getSelector();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
