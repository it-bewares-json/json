/**
 *
 * Copyright 2017-2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.tree;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import it.bewares.json.JSONFactory;

/**
 * This class is a wrapper for a {@link java.lang.String} and represents it in
 * JSON. A {@code JSONString} contains only the represented {@code String}.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
@Slf4j
public class JSONString implements JSONPrimitive<JSONString>, CharSequence {

    private static final long serialVersionUID = -4867856388981944774L;

    @Getter
    @Setter
    private String value;

    /**
     * Allocates a {@code JSONString} object wrapping the {@code string}
     * argument.
     *
     * @param string corresponding String
     */
    public JSONString(String string) {
        this.value = string;
    }

    /**
     * Allocates a {@code JSONString} object wrapping a String represented by
     * the respective {@code CharSequence cs} argument.
     *
     * @param cs CharSequence representing the corresponding String
     */
    public JSONString(CharSequence cs) {
        this(cs.toString());
    }

    /**
     * Allocates a {@code JSONString} by default wrapping the empty String.
     */
    public JSONString() {
        this("");
    }

    /**
     * Returns the length of the wrapped sequence.
     *
     * @return the number of {@code char}s in the wrapped sequence
     */
    @Override
    public int length() {
        return this.getValue().length();
    }

    /**
     * Returns the {@code char} value at the specified index of the wrapped
     * sequence. An index ranges from 0 to {@code value.length() - 1}.
     *
     * @param index the index of the {@code char} value to be returned
     * @return the specified {@code char} value at the given index
     * @throws IndexOutOfBoundsException if the {@code index} argument is
     * negative or not less than {@code length()}
     */
    @Override
    public char charAt(int index) {
        return this.getValue().charAt(index);
    }

    /**
     * Returns a {@code CharSequence} that is a subsequence of the wrapped
     * sequence. The subsequence starts at the specified index {@code start} and
     * ends at index {@code end - 1}.
     *
     * @param start start index (inclusive)
     * @param end end index (exclusive)
     * @return a subsequence between {@code start} and {@code end}
     * @throws IndexOutOfBoundsException if {@code start} are {@code end} not in
     * the range of 0 and {@code value.length() - 1} or if {@code start} is
     * greater than {@code end}
     */
    @Override
    public CharSequence subSequence(int start, int end) {
        return this.getValue().subSequence(start, end);
    }

    /**
     * Compares the value of this object with another {@code JSONString}.
     *
     * @param b {@code JSONString} to be compared with
     * @return integer is negative if this value lexicographically comes before
     * b's value, zero if both have the same value and positive if this value
     * lexicographically comes after b's value
     */
    @Override
    public int compareTo(JSONString b) {
        return this.getValue().compareTo(b.getValue());
    }

    /**
     * Returns the index within the wrapped String of the first occurrence of
     * the specified character. If no such character occurs in that string, then
     * {@code -1} is returned.
     *
     * @param c character to be searched for
     * @return the index of the first occurrence of the character, or {@code -1}
     * if the character does not occur
     */
    public int indexOf(int c) {
        return this.getValue().indexOf(c);
    }

    /**
     * Returns the index within the wrapped String of the first occurrence of
     * the specified character from the index upwards. If no such character
     * occurs in that string after the specified index, then {@code -1} is
     * returned.
     *
     * @param c character to be searched for
     * @param from the index to start from
     * @return the index of the first occurrence of the character after the
     * specified index, or {@code -1} if the character does not occur
     */
    public int indexOf(int c, int from) {
        return this.getValue().indexOf(c, from);
    }

    /**
     * Returns the index within the wrapped String of the first occurrence of
     * the specified String. If the specified String does not occur in that
     * String, then {@code -1} is returned.
     *
     * @param s string to be searched for
     * @return the index of the first occurrence of the specified String, or
     * {@code -1} if the character does not occur
     */
    public int indexOf(String s) {
        return this.getValue().indexOf(s);
    }

    /**
     * Returns the index within the wrapped String of the first occurrence of
     * the specified String from the index upwards. If the specified String does
     * not occur in that String after the specified index, then {@code -1} is
     * returned.
     *
     * @param s string to be searched for
     * @param from the index to start from
     * @return the index of the first occurrence of the specified String after
     * the specified index, or {@code -1} if the character does not occur
     */
    public int indexOf(String s, int from) {
        return this.getValue().indexOf(s, from);
    }

    /**
     * Returns the index within the wrapped String of the last occurrence of the
     * specified character. If no such character occurs in that String, then
     * {@code -1} is returned.
     *
     * @param c character to be searched for
     * @return the index of the last occurrence of the specified String after
     * the specified index, or {@code -1} if the character does not occur
     */
    public int lastIndexOf(int c) {
        return this.getValue().lastIndexOf(c);
    }

    /**
     * Returns the index within the wrapped String of the last occurrence of the
     * specified character from the index upwards. If no such character occurs
     * in that string after the specified index, then {@code -1} is returned.
     *
     * @param c character to be searched for
     * @param from the index to start from
     * @return the index of the last occurrence of the specified String after
     * the specified index, or {@code -1} if the character does not occur
     */
    public int lastIndexOf(int c, int from) {
        return this.getValue().lastIndexOf(c, from);
    }

    /**
     * Returns the index within the wrapped String of the last occurrence of the
     * specified String from the index upwards. If the specified String does not
     * occur in that String, then {@code -1} is returned.
     *
     * @param s string to be searched for
     * @return the index of the last occurrence of the specified String after
     * the specified index, or {@code -1} if the character does not occur
     */
    public int lastIndexOf(String s) {
        return this.getValue().lastIndexOf(s);
    }

    /**
     * Returns the index within the wrapped String of the last occurrence of the
     * specified String from the index upwards. If the specified String does not
     * occur in that String after the specified index, then {@code -1} is
     * returned.
     *
     * @param s string to be searched for
     * @param from the index to start from
     * @return the index of the last occurrence of the specified String after
     * the specified index, or {@code -1} if the character does not occur
     */
    public int lastIndexOf(String s, int from) {
        return this.getValue().lastIndexOf(s, from);
    }

    /**
     * Compares this object with another Object to check if both are
     * {@code JSONString}s and if their wrapped {@code String}s are equal.
     *
     * @param b Object to be checked against
     * @return a {@code boolean}, {@code true} if o is a {@code JSONString} and
     * both wrapped values are equal, otherwise {@code false}
     */
    @Override
    public boolean equals(Object b) {
        if (b instanceof JSONString) {
            return this.getValue().equals(((JSONString) b).getValue());
        }
        return false;
    }

    /**
     * Calculates a hash code value for this {@code JSONString} based on its
     * wrapped {@code String} hash code.
     *
     * @return a hash code value for this {@code JSONString}
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.getValue());
        return hash;
    }

    /**
     * Compares this {@code JSONString} with another {@code JSONString} ignoring
     * case.
     *
     * @param b JSONString to be compared with
     * @return if the wrapped Strings are equivalent ignoring case
     */
    public boolean equalsIgnoreCase(JSONString b) {
        return this.getValue().equalsIgnoreCase(b.getValue());
    }

    /**
     * Unescapes a given for JSON escaped String {@code input}.
     *
     * @param input String to be unescaped
     * @return unescaped input String
     * @throws IllegalArgumentException when the input was not escaped properly
     */
    public static String unescape(String input) {
        if (Character.toString(JSONFactory.STRING_SURROUND).equals(input) || !input.startsWith(Character.toString(JSONFactory.STRING_SURROUND)) || !input.endsWith(Character.toString(JSONFactory.STRING_SURROUND))) {
            throw new IllegalArgumentException(new JSONString(input).toString() + " is not an escaped json string");
        }

        String output = input.substring(1, input.length() - 1)
                .replace("\\t", "\t")
                .replace("\\r", "\r")
                .replace("\\n", "\n")
                .replace("\\f", "\f")
                .replace("\\b", "\b")
                .replace("\\\"", "\"")
                .replace("\\/", "/")
                .replace("\\\\", "\\");

        Matcher match = Pattern.compile("\\\\u([\\da-fA-F]{4})").matcher(output);
        while (match.find()) {
            String unescaped = Character.toString((char) Integer.parseInt(match.group(1), 16));

            LOG.debug("unescape() found {} with unicode escaped char from {} to {} in \"{}\"", input, match.start(), match.end(), output);

            output = output.substring(0, match.start())
                    + unescaped
                    + output.substring(match.end());

            match = match.pattern().matcher(output);
        }

        return output;

    }

    /**
     * Returns the wrapped String represented by this {@code JSONString}.
     *
     * @return the wrapped String
     */
    @Override
    public String toString() {
        return this.getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JSONFactory.Type type() {
        return JSONFactory.Type.STRING;
    }

}
