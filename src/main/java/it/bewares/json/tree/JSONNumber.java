/**
 *
 * Copyright 2017-2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.tree;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Objects;
import lombok.Getter;
import lombok.Setter;
import it.bewares.json.JSONFactory;

/**
 * This class is a wrapper for a {@link java.math.BigDecimal} and represents a
 * {@link java.lang.Number} in JSON. A {@code JSONNumber} contains only the
 * represented {@code Number} in form of a {@code BigDecimal}.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
public class JSONNumber extends Number implements JSONPrimitive<JSONNumber> {

    private static final long serialVersionUID = -3264242497978862050L;

    @Getter
    @Setter
    private BigDecimal value;

    /**
     * Allocates a {@code JSONNumber} object wrapping the {@code number}
     * argument.
     *
     * @param number corresponding Number
     */
    public JSONNumber(Number number) {
        BigDecimal value;

        if (number instanceof JSONNumber) {
            value = ((JSONNumber) number).getValue();
        } else if (number instanceof BigDecimal) {
            value = (BigDecimal) number;
        } else if (number instanceof Integer) {
            value = new BigDecimal((int) number);
        } else if (number instanceof Float) {
            value = new BigDecimal((float) number);
        } else if (number instanceof Double) {
            value = new BigDecimal((double) number);
        } else if (number instanceof Long) {
            value = new BigDecimal((long) number);
        } else if (number instanceof BigInteger) {
            value = new BigDecimal((BigInteger) number);
        } else {
            throw new IllegalArgumentException();
        }

        this.value = value;
    }

    /**
     * Allocates a {@code JSONNumber} object wrapping a {@code BigDecimal} which
     * is represented by the JSON escaped String {@code input}.
     *
     * @param input String to be converted to the corresponding Number
     */
    public JSONNumber(String input) {
        this.parseAndSetValue(input);
    }

    /**
     * Allocates a {@code JSONNumber} by default wrapping the
     * {@code BigDecimal.ZERO}.
     */
    public JSONNumber() {
        this(BigDecimal.ZERO);
    }

    /**
     * Returns the value of the wrapped Number as an {@code int}.
     *
     * @return the rounded numeric value as an {@code int}
     */
    @Override
    public int intValue() {
        return this.getValue().intValue();
    }

    /**
     * Returns the value of the wrapped Number as a {@code long}.
     *
     * @return the rounded numeric value as a {@code long}
     */
    @Override
    public long longValue() {
        return this.getValue().longValue();
    }

    /**
     * Returns the value of the wrapped Number as a {@code float}.
     *
     * @return the numeric value as a {@code float}
     */
    @Override
    public float floatValue() {
        return this.getValue().floatValue();
    }

    /**
     * Returns the value of the wrapped Number as a {@code double}.
     *
     * @return the numeric value as a {@code double}
     */
    @Override
    public double doubleValue() {
        return this.getValue().doubleValue();
    }

    /**
     * Returns the value of the wrapped Number as a {@code byte}.
     *
     * @return the rounded numeric value as a {@code byte}
     */
    @Override
    public byte byteValue() {
        return this.getValue().byteValue();
    }

    /**
     * Returns the value of the wrapped Number as a {@code short}.
     *
     * @return the rounded numeric value as a {@code short}
     */
    @Override
    public short shortValue() {
        return this.getValue().shortValue();
    }

    /**
     * Sets the wrapped BigDecimals value to an parsed value of a given input
     * String representing a Number.
     *
     * @param input String representing the new wrapped Number
     * @throws NumberFormatException when the input was no valid Number
     * representative
     */
    public void parseAndSetValue(String input) {
        this.setValue(new BigDecimal(input));
    }

    /**
     * Compares the value of this object with another {@code JSONNumber}.
     *
     * @param b JSONNumber to be compared with
     * @return integer is negative if this value is numerically less, zero if it
     * is numerically equal and positive if it is numerically greater then b's
     * value
     */
    @Override
    public int compareTo(JSONNumber b) {
        return this.getValue().compareTo(b.getValue());
    }

    /**
     * Compares this object with another Object to check if both are
     * {@code JSONNumber}s and if their wrapped {@code BigDecimal}s are
     * numerically equal.
     *
     * @param o Object to be checked against
     * @return a {@code boolean}, {@code true} if o is a {@code JSONNumber} and
     * both wrapped numbers are numerically equal, otherwise {@code false}
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof JSONNumber) {
            return this.compareTo((JSONNumber) o) == 0;
        }
        return false;
    }

    /**
     * Calculates a hash code value for this {@code JSONNumber} based on its
     * wrapped {@code BigDecimal} hash code.
     *
     * @return a hash code value for this {@code JSONNumber}
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.value);
        return hash;
    }

    /**
     * Returns a String representation of this {@code JSONNumber}. It is the
     * wrapped {@code BigDecimal}s representation.
     *
     * @return a String representation
     */
    @Override
    public String toString() {
        return this.value.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JSONFactory.Type type() {
        return JSONFactory.Type.NUMBER;
    }

    /**
     * Determines if this {@code JSONNumber} is greater than the specified
     * {@code JSONNumber}.
     *
     * @param b the {@code JSONNumber} to be compared with
     * @return {@code true} if this {@code JSONNumber} is greater than the
     * specified {@code JSONNumber}, otherwise {@code false}
     */
    public boolean greaterThan(JSONNumber b) {
        return this.compareTo(new JSONNumber(b)) > 0;
    }

    /**
     * Determines if this {@code JSONNumber} is less than the specified
     * {@code JSONNumber}.
     *
     * @param b the {@code JSONNumber} to be compared with
     * @return {@code true} if this {@code JSONNumber} is less than the
     * specified {@code JSONNumber}, otherwise {@code false}
     */
    public boolean lessThan(JSONNumber b) {
        return this.compareTo(new JSONNumber(b)) < 0;
    }

    /**
     * Determines if this {@code JSONNumber} is greater than or equal to the
     * specified {@code JSONNumber}.
     *
     * @param b the {@code JSONNumber} to be compared with
     * @return {@code true} if this {@code JSONNumber} is greater than or equal
     * to the specified {@code JSONNumber}, otherwise {@code false}
     */
    public boolean greaterThanOrEquals(JSONNumber b) {
        return this.compareTo(new JSONNumber(b)) >= 0;
    }

    /**
     * Determines if this {@code JSONNumber} is less than or equal to the
     * specified {@code JSONNumber}.
     *
     * @param b the {@code JSONNumber} to be compared with
     * @return {@code true} if this {@code JSONNumber} is less than or equal to
     * the specified {@code JSONNumber}, otherwise {@code false}
     */
    public boolean lessThanOrEquals(JSONNumber b) {
        return this.compareTo(b) <= 0;
    }

}
