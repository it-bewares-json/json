/**
 *
 * Copyright 2017-2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.tree;

import it.bewares.json.JSONFactory;
import java.io.Serializable;

/**
 * All implementations of this Interface represent a values in JSON. That
 * contains therefor objects, arrays, strings, numbers, booleans and in theory
 * {@code null}.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
public interface JSONValue extends Cloneable, Serializable {

    /**
     * Returns the String representation of this {@code JSONValue}.
     *
     * @return a String representation
     */
    @Override
    public String toString();

    /**
     * Returns a copy of the original {@code JSONValue}.
     *
     * @return a copy of the original {@code JSONValue}
     * @throws java.lang.CloneNotSupportedException if the cloning process
     * failed
     */
    public Object clone() throws CloneNotSupportedException;

    /**
     * Returns the type of the {@code JSONValue}.
     *
     * @return the type of the {@code JSONValue}
     */
    public JSONFactory.Type type();

}
