/**
 *
 * Copyright 2017-2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.tree;

/**
 * All implementations of this Interface represent a primitive in JSON. That
 * contains strings, numbers, booleans and in theory {@code null}. JSON
 * primitive shall not be confused with primitives in the Java programming
 * language.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 * @param <T> the type of the JSON primitive itself so instances of it can be
 * compared
 */
public interface JSONPrimitive<T extends JSONPrimitive> extends JSONValue, Comparable<T> {

}
