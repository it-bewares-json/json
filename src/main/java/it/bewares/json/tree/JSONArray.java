/**
 *
 * Copyright 2017-2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.tree;

import it.bewares.json.JSONFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import javax.inject.Inject;
import lombok.Setter;

/**
 * This class is a wrapper for a {@link java.util.List} and represents it in
 * JSON. A {@code JSONArray} contains only the represented {@code List}.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 * @param <E> the type of elements in this list which has to be a
 * {@code JSONStructure}
 */
public class JSONArray<E extends JSONValue> implements JSONStructure, List<E> {

    private static final long serialVersionUID = -2114996387697314365L;

    @Setter
    @Inject
    private JSONFactory factory = null;

    private final ArrayList<E> underlyingList;

    /**
     * Allocates a {@code JSONArray} by default wrapping an empty {@code List}.
     */
    public JSONArray() {
        this.underlyingList = new ArrayList<>();
    }

    /**
     * Allocates a {@code JSONArray} wrapping the given {@code collection}
     * argument.
     *
     * @param collection corresponding Collection
     */
    public JSONArray(Collection collection) {
        this();

        this.addAll(collection);
    }

    /**
     * Allocates a {@code JSONArray} containing the specified {@code entries}.
     *
     * @param entries JSON values to be contained by the corresponding List
     */
    public JSONArray(JSONValue... entries) {
        this(Arrays.asList(entries));
    }

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list
     */
    @Override
    public int size() {
        return this.underlyingList.size();
    }

    /**
     * Determines if this map contains elements.
     *
     * @return {@code true} if this list contains no elements, otherwise
     * {@code false}
     */
    @Override
    public boolean isEmpty() {
        return this.underlyingList.isEmpty();
    }

    /**
     * Determines if this map contains the specified element.
     *
     * @param element element whose presence in this list shall be tested
     * @return {@code true} if this list contains the element, otherwise
     * {@code false}
     */
    @Override
    public boolean contains(Object element) {
        return this.underlyingList.contains(this.getFactory().createValue(element));
    }

    /**
     * Returns an iterator over the elements in this list in proper sequence.
     *
     * @return an iterator over the elements in this list in proper sequence
     */
    @Override
    public Iterator<E> iterator() {
        return this.underlyingList.iterator();
    }

    /**
     * Returns an array containing all of the elements in this list in proper
     * sequence.
     *
     * @return an array containing all of the elements in this list in proper
     * sequence
     */
    @Override
    public Object[] toArray() {
        return this.underlyingList.toArray();
    }

    /**
     * Returns an array which type is that of the specified array containing all
     * of the elements in this list in proper sequence.
     *
     * @param <T> the demanded type for the returned array
     * @param array the array into which the elements of this list shall be
     * stored
     * @return an array which type is that of the specified array containing all
     * of the elements in this list in proper sequence
     */
    @Override
    public <T> T[] toArray(T[] array) {
        return this.underlyingList.toArray(array);
    }

    /**
     * Appends the specified element to the end of this list.
     *
     * @param element element to be appended to this list
     * @return {@code true} if it was successfully appended
     */
    @Override
    public boolean add(E element) {
        return this.underlyingList.add(element);
    }

    /**
     * Appends the specified Map element to the end of this list.
     *
     * @param element map to be appended as JSONObject to this list
     * @return {@code true} if it was successfully appended
     */
    public boolean add(Map element) {
        return this.add(this.getFactory().<E>createValue(element));
    }

    /**
     * Appends the specified List element to the end of this list.
     *
     * @param element list to be appended as JSONArray to this list
     * @return {@code true} if it was successfully appended
     */
    public boolean add(List element) {
        return this.add(this.getFactory().<E>createValue(element));
    }

    /**
     * Appends the specified List element to the end of this list.
     *
     * @param element String to be appended as JSONString to this list
     * @return {@code true} if it was successfully appended
     */
    public boolean add(String element) {
        return this.add(this.getFactory().<E>createValue(element));
    }

    /**
     * Appends the specified Boolean element to the end of this list.
     *
     * @param element boolean to be appended as JSONBoolean to this list
     * @return {@code true} if it was successfully appended
     */
    public boolean add(Boolean element) {
        return this.add(this.getFactory().<E>createValue(element));
    }

    /**
     * Appends the specified Number element to the end of this list.
     *
     * @param element number to be appended as JSONNumber to this list
     * @return {@code true} if it was successfully appended
     */
    public boolean add(Number element) {
        return this.add(this.getFactory().<E>createValue(element));
    }

    /**
     * Removes the first occurrence of the specified element from this list.
     *
     * @param element element to be removed from this list
     * @return {@code true} if it was successfully removed
     */
    @Override
    public boolean remove(Object element) {
        return this.underlyingList.remove(this.getFactory().createValue(element));
    }

    /**
     * Determines if this map contains all of the elements of the specified
     * collection.
     *
     * @param collection collection which shall be checked for containment in
     * this list
     * @return {@code true} if this list contains all of the elements of the
     * specified collection, otherwise {@code false}
     */
    @Override
    public boolean containsAll(Collection<?> collection) {
        return this.underlyingList.containsAll(new JSONArray<E>(collection));
    }

    /**
     * Appends all of the elements of the specified collection to the end of
     * this list in the order that they are returned by the specified
     * collection's iterator.
     *
     * @param collection collection containing elements to be appended to this
     * list
     * @return {@code true} if they were successfully appended
     */
    @Override
    public boolean addAll(Collection collection) {
        Collection<E> d = new ArrayList<>();
        for (Object element : collection) {
            d.add(this.getFactory().<E>createValue(element));
        }

        return this.underlyingList.addAll(d);
    }

    /**
     * Inserts all of the elements of the specified collection at the specified
     * index in the order that they are returned by the specified collection's
     * iterator.
     *
     * @param index index at which to insert the first element from the
     * specified collection
     * @param collection collection containing elements to be appended to this
     * list
     * @return {@code true} if they were successfully inserted
     */
    @Override
    public boolean addAll(int index, Collection collection) {
        return this.underlyingList.addAll(index, new JSONArray<E>(collection));
    }

    /**
     * Removes the first occurrence of all of the elements of the specified
     * collection from this list.
     *
     * @param collection collection containing elements to be removed from this
     * list
     * @return {@code true} if they were successfully removed
     */
    @Override
    public boolean removeAll(Collection<?> collection) {
        return this.underlyingList.removeAll(new JSONArray<E>(collection));
    }

    /**
     * Retains only the elements in this collection that are contained in the
     * specified collection. Removes therefor all other elements not from this
     * list which are not contained in the specified collection.
     *
     * @param collection collection containing elements to be retained in this
     * collection
     * @return {@code true} if all other elements were successfully removed
     */
    @Override
    public boolean retainAll(Collection<?> collection) {
        return this.underlyingList.retainAll(new JSONArray<E>(collection));
    }

    /**
     * Removes all of the elements from this list, such that it will be empty.
     */
    @Override
    public void clear() {
        this.underlyingList.clear();
    }

    /**
     * Returns the value corresponding to the specified index.
     *
     * @param index index of the element which shall be returned
     * @return the value corresponding to the specified index
     * @throws IndexOutOfBoundsException if the index is out of range
     */
    @Override
    public E get(int index) {
        return this.underlyingList.get(index);
    }

    /**
     * Replaces the element at the specified index with the specified element.
     *
     * @param index index for the replacement
     * @param element element to replace
     * @return {@code true} if it was successfully set
     */
    @Override
    public E set(int index, E element) {
        return this.underlyingList.set(index, element);
    }

    /**
     * Replaces the element at the specified index with the specified Map
     * element.
     *
     * @param index index for the replacement
     * @param element map to replace as a JSONObject
     * @return {@code true} if it was successfully set
     */
    public E set(int index, Map element) {
        return this.set(index, this.getFactory().<E>createValue(element));
    }

    /**
     * Replaces the element at the specified index with the specified List
     * element.
     *
     * @param index index for the replacement
     * @param element list to replace as a JSONArray
     * @return {@code true} if it was successfully set
     */
    public E set(int index, List element) {
        return this.set(index, this.getFactory().<E>createValue(element));
    }

    /**
     * Replaces the element at the specified index with the specified String
     * element.
     *
     * @param index index for the replacement
     * @param element String to replace as a JSONString
     * @return {@code true} if it was successfully set
     */
    public E set(int index, String element) {
        return this.set(index, this.getFactory().<E>createValue(element));
    }

    /**
     * Replaces the element at the specified index with the specified Boolean
     * element.
     *
     * @param index index for the replacement
     * @param element boolean to replace as a JSONBoolean
     * @return {@code true} if it was successfully set
     */
    public E set(int index, Boolean element) {
        return this.set(index, this.getFactory().<E>createValue(element));
    }

    /**
     * Replaces the element at the specified index with the specified Number
     * element.
     *
     * @param index index at which to replace
     * @param element number to replace as a JSONNumber
     * @return {@code true} if it was successfully set
     */
    public E set(int index, Number element) {
        return this.set(index, this.getFactory().<E>createValue(element));
    }

    /**
     * Inserts the specified element at the specified index. Elements at this an
     * the following indeces are therefor shifted one index forward.
     *
     * @param index index for the insertation
     * @param element element which shall be inserted
     */
    @Override
    public void add(int index, E element) {
        this.underlyingList.add(index, element);
    }

    /**
     * Inserts the specified Map element at the specified index. Elements at
     * this an the following indeces are therefor shifted one index forward.
     *
     * @param index index for the insertation
     * @param element map which shall be inserted as a JSONObject
     */
    public void add(int index, Map element) {
        this.add(index, this.getFactory().<E>createValue(element));
    }

    /**
     * Inserts the specified List element at the specified index. Elements at
     * this an the following indeces are therefor shifted one index forward.
     *
     * @param index index for the insertation
     * @param element list which shall be inserted as a JSONArray
     */
    public void add(int index, List element) {
        this.add(index, this.getFactory().<E>createValue(element));
    }

    /**
     * Inserts the specified String element at the specified index. Elements at
     * this an the following indeces are therefor shifted one index forward.
     *
     * @param index index for the insertation
     * @param element String which shall be inserted as a JSONString
     */
    public void add(int index, String element) {
        this.add(index, this.getFactory().<E>createValue(element));
    }

    /**
     * Inserts the specified Boolean element at the specified index. Elements at
     * this an the following indeces are therefor shifted one index forward.
     *
     * @param index index for the insertation
     * @param element boolean which shall be inserted as a JSONBoolean
     */
    public void add(int index, Boolean element) {
        this.add(index, this.getFactory().<E>createValue(element));
    }

    /**
     * Inserts the specified Number element at the specified index. Elements at
     * this an the following indeces are therefor shifted one index forward.
     *
     * @param index index for the insertation
     * @param element number which shall be inserted as a JSONNumber
     */
    public void add(int index, Number element) {
        this.add(index, this.getFactory().<E>createValue(element));
    }

    /**
     * Removes the element at the specified index in this list. Elements at this
     * an the following indeces are therefor shifted one index back.
     *
     * @param index the index of the element to be removed
     * @return the element previously at the specified index
     */
    @Override
    public E remove(int index) {
        return this.underlyingList.remove(index);
    }

    /**
     * Determines the index of the first occurrence of the specified Object.
     *
     * @param o element to search for
     * @return either the index of the first occurrence of the specified object
     * or -1
     */
    @Override
    public int indexOf(Object o) {
        return this.underlyingList.indexOf(this.getFactory().createValue(o));
    }

    /**
     * Determines the index of the last occurrence of the specified Object.
     *
     * @param o element to search for
     * @return either the index of the last occurrence of the specified object
     * or -1
     */
    @Override
    public int lastIndexOf(Object o) {
        return this.underlyingList.lastIndexOf(this.getFactory().createValue(o));
    }

    /**
     * Returns a list iterator over the elements in this list (in proper
     * sequence).
     *
     * @return a list iterator over the elements in this list (in proper
     * sequence)
     */
    @Override
    public ListIterator<E> listIterator() {
        return this.underlyingList.listIterator();
    }

    /**
     * Returns a list iterator over the elements in this list (in proper
     * sequence), starting at the specified position in the list. Elements at
     * indeces before the specified index at index -1 and downwards.
     *
     * @param index index of the first element to be returned from the list
     * iterator
     * @return a list iterator over the elements in this list (in proper
     * sequence), starting at the specified position in the list
     */
    @Override
    public ListIterator<E> listIterator(int index) {
        return this.underlyingList.listIterator(index);
    }

    /**
     * Returns a list containing the elements in the specified range between
     * including the specified {@code fromIndex} and {@code toIndex - 1}. The
     * set is backed by the map, so changes to the map are reflected in the set,
     * and vice-versa.
     *
     * @param fromIndex low endpoint (inclusive) of the subList
     * @param toIndex high endpoint (exclusive) of the subList
     * @return a list containing the elements in the specified range
     */
    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return new JSONArray<E>(this.underlyingList.subList(fromIndex, toIndex));
    }

    /**
     * Returns the {@code JSONFactory} that is currently in used for connection
     * with different modules.
     *
     * If there is none, one will be instanciated.
     *
     * @return the {@code JSONFactory} that is currently in use
     */
    public JSONFactory getFactory() {
        if (this.factory == null) {
            this.setFactory(new JSONFactory());
        }

        return this.factory;
    }

    /**
     * Compares this object with another Object to check if both are
     * {@code JSONArray}s and if their wrapped {@code List}s are equal.
     *
     * @param o Object to be checked against
     * @return a {@code boolean}, {@code true} if o is a {@code JSONArray} and
     * both wrapped Lists are equal, otherwise {@code false}
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof JSONArray) {
            return this.underlyingList.equals(((JSONArray) o).underlyingList);
        }
        return false;
    }

    /**
     * Calculates a hash code value for this {@code JSONArray} based on its
     * wrapped {@code List}s hash code.
     *
     * @return a hash code value for this {@code JSONArray}
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.underlyingList);
        return hash;
    }

    /**
     * Returns the String representation of this {@code JSONArray}.
     *
     * @return a String representation
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("JSONArray[");

        int counter = this.size();
        for (E element : this) {
            if (element == null) {
                sb.append("null");
            } else if (element.type().isStructure()) {
                sb.append(JSONFactory.Type.OBJECT.equals(element.type()) ? "{...}" : "[...]");
            } else if (JSONFactory.Type.STRING.equals(element.type())) {
                sb.append("\"...\"");
            } else {
                sb.append(Objects.toString(element));
            }
            if (--counter <= 0) {
                sb.append(']');
            } else {
                sb.append(", ");
            }
        }

        return sb.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JSONFactory.Type type() {
        return JSONFactory.Type.ARRAY;
    }

}
