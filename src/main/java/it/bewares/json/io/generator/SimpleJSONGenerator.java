/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.io.generator;

import java.util.Iterator;
import java.util.Map;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import it.bewares.json.JSONFactory;
import it.bewares.json.io.exception.JSONFormatException;
import it.bewares.json.tree.JSONArray;
import it.bewares.json.tree.JSONBoolean;
import it.bewares.json.tree.JSONNumber;
import it.bewares.json.tree.JSONObject;
import it.bewares.json.tree.JSONString;
import it.bewares.json.tree.JSONValue;
import java.io.IOException;
import java.io.Writer;

/**
 * This JSON generator is meant to be a minimalistic generator without any
 * extensions or optimizations for spicific situations.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
@Slf4j
@Singleton
public class SimpleJSONGenerator implements JSONGenerator {

    /**
     * {@inheritDoc}
     */
    @Override
    public void write(JSONValue json, Writer writer) throws JSONFormatException, IOException {
        LOG.debug("write() generating json text");

        if (json == null) {
            LOG.debug("write() having json null");

            writer.append("null");
        } else {
            JSONFactory.Type type = json.type();

            switch (type) {
                case ARRAY:
                    LOG.debug("write() generating json array text");

                    writer.append(JSONFactory.ARRAY_START);

                    for (Iterator<? extends JSONValue> iterator = ((JSONArray<? extends JSONValue>) json).iterator(); iterator.hasNext();) {
                        JSONValue element = iterator.next();

                        this.write(element, writer);

                        if (iterator.hasNext()) {
                            writer.append(JSONFactory.SEPARATOR);
                        }
                    }

                    writer.append(JSONFactory.ARRAY_END);
                    break;

                case OBJECT:
                    LOG.debug("write() generating json object text");

                    writer.append(JSONFactory.OBJECT_START);

                    for (Iterator<Map.Entry<String, JSONValue>> iterator = ((JSONObject) json).entrySet().iterator(); iterator.hasNext();) {
                        Map.Entry entry = iterator.next();

                        JSONString key = new JSONString((String) entry.getKey());
                        JSONValue value = (JSONValue) entry.getValue();

                        this.write(key, writer);

                        writer.append(JSONFactory.KEY_VALUE_SEPARATOR);

                        this.write(value, writer);

                        if (iterator.hasNext()) {
                            writer.append(JSONFactory.SEPARATOR);
                        }
                    }

                    writer.append(JSONFactory.OBJECT_END);
                    break;

                case STRING:
                    LOG.debug("write() generating json string text");

                    writer.append(JSONFactory.STRING_SURROUND);

                    for (char c : ((JSONString) json).getValue().toCharArray()) {
                        switch (c) {
                            case JSONFactory.STRING_SURROUND:
                            case '\\':
                            case '/':
                                writer.append(JSONFactory.STRING_ESCAPE);
                                writer.append(c);
                                break;

                            case '\b':
                                writer.append("\\b");
                                break;

                            case '\f':
                                writer.append("\\f");
                                break;

                            case '\n':
                                writer.append("\\n");
                                break;

                            case '\r':
                                writer.append("\\r");
                                break;

                            case '\t':
                                writer.append("\\t");
                                break;

                            default:
                                if (c <= '\u001F') {
                                    String escaped = Integer.toHexString((int) c);
                                    writer.append("\\u000".substring(0, 6 - escaped.length())).append(escaped);
                                } else {
                                    writer.append(c);
                                }
                                break;
                        }
                    }

                    writer.append(JSONFactory.STRING_SURROUND);
                    break;

                case NUMBER:
                    LOG.debug("write() generating json number text");

                    writer.append(((JSONNumber) json).toString());
                    break;

                case BOOLEAN:
                    LOG.debug("write() generating json boolean text");

                    writer.append(((JSONBoolean) json).toString());
                    break;

                default:
                    throw new IllegalArgumentException("input is not valid");
            }
        }
    }

}
