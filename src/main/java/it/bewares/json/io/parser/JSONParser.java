/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.io.parser;

import java.io.IOException;
import java.io.InputStream;
import it.bewares.json.io.exception.JSONFormatException;
import it.bewares.json.tree.JSONValue;
import java.io.ByteArrayOutputStream;
import java.io.Reader;

/**
 * All implementations of this Interface shall parse valid JSON. The purpose of
 * this interface is to give more flexibility and to let applications use or
 * implement their own Parser which may be optimized for specific tasks and may
 * parse JSON extensions.
 *
 * Implementations should use the {@code @Singleton} annotation to make a
 * dependency injection possible.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
public interface JSONParser {

    /**
     * Parses the given input and returns the {@code JSONValue} representative.
     *
     * @param input String to be parsed
     * @return JSONValue representative of the input
     * @throws JSONFormatException if the input is not valid JSON
     */
    public JSONValue parse(String input) throws JSONFormatException;

    /**
     * Parses the given input and returns the {@code JSONValue} representative.
     *
     * By default the InputStream is fully read in and then passed over to
     * String parsing, however it is highly recommended to implement an own
     * stream parse method.
     *
     * @param input InputStream to be parsed
     * @return {@code JSONValue} representative of the input
     * @throws JSONFormatException if the input is not valid JSON
     * @throws java.io.IOException if the InputStream does
     */
    default public JSONValue parse(InputStream input) throws JSONFormatException, IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }

        return this.parse(output.toString("utf-8"));
    }

    /**
     * Parses the given input and returns the {@code JSONValue} representative.
     *
     * By default the Reader is fully read in and then passed over to String
     * parsing, however it is highly recommended to implement an own reader
     * parse method.
     *
     * @param input Reader to be parsed
     * @return {@code JSONValue} representative of the input
     * @throws JSONFormatException if the input is not valid JSON
     * @throws java.io.IOException if the Reader does
     */
    default public JSONValue parse(Reader input) throws JSONFormatException, IOException {
        StringBuilder builder = new StringBuilder();

        int charsRead = -1;
        char[] chars = new char[100];
        do {
            charsRead = input.read(chars, 0, chars.length);
            if (charsRead > 0) {
                builder.append(chars, 0, charsRead);
            }
        } while (charsRead > 0);

        return this.parse(builder.toString());
    }

}
