/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.io.parser;

import it.bewares.json.JSONFactory;
import it.bewares.json.io.exception.JSONFormatException;
import it.bewares.json.tree.JSONArray;
import it.bewares.json.tree.JSONBoolean;
import it.bewares.json.tree.JSONNumber;
import it.bewares.json.tree.JSONObject;
import it.bewares.json.tree.JSONString;
import it.bewares.json.tree.JSONValue;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

/**
 * This JSON parse is meant to be optimized for parsing situations with large
 * JSON inputs delivered by an {@code Reader} without any extensions.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC
 *
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
@Slf4j
@Singleton
public class JSONStreamParser implements JSONParser {

    private static Set<Character> WHITESPACE_CHARACTERS = new HashSet<>(Arrays.asList(' ', '\t', '\n', '\r'));

    private JSONObject object(Reader inputReader) throws IOException {
        if (inputReader.read() != JSONFactory.OBJECT_START) {
            throw new JSONFormatException();
        }

        JSONObject<JSONValue> json = new JSONObject<>();

        while (true) {
            this.whitespace(inputReader);

            inputReader.mark(1);
            char next = (char) inputReader.read();
            if (next == JSONFactory.OBJECT_END) {
                break;
            }
            inputReader.reset();

            JSONString key = this.string(inputReader);

            this.whitespace(inputReader);

            if (inputReader.read() != JSONFactory.KEY_VALUE_SEPARATOR) {
                throw new JSONFormatException();
            }

            this.whitespace(inputReader);

            JSONValue value = this.delegate(inputReader);

            json.put(key, value);

            this.whitespace(inputReader);

            next = (char) inputReader.read();
            if (next == JSONFactory.OBJECT_END) {
                break;
            } else if (next != JSONFactory.SEPARATOR) {
                throw new JSONFormatException();
            }
        }

        return json;
    }

    private JSONArray array(Reader inputReader) throws IOException {
        if (inputReader.read() != JSONFactory.ARRAY_START) {
            throw new JSONFormatException();
        }

        JSONArray<JSONValue> json = new JSONArray<>();

        while (true) {
            this.whitespace(inputReader);

            inputReader.mark(1);
            char next = (char) inputReader.read();
            if (next == JSONFactory.ARRAY_END) {
                break;
            }
            inputReader.reset();

            JSONValue element = this.delegate(inputReader);

            json.add(element);

            this.whitespace(inputReader);

            next = (char) inputReader.read();
            if (next == JSONFactory.ARRAY_END) {
                break;
            } else if (next != JSONFactory.SEPARATOR) {
                throw new JSONFormatException();
            }
        }

        return json;
    }

    private JSONString string(Reader inputReader) throws IOException {
        if (inputReader.read() != JSONFactory.STRING_SURROUND) {
            throw new JSONFormatException();
        }

        StringBuilder builder = new StringBuilder();

        int next;

        while (true) {
            next = inputReader.read();

            if (next == -1) {
                throw new JSONFormatException();
            }
            if (next == JSONFactory.STRING_SURROUND) {
                break;
            }

            if (next == '\\') {
                switch ((char) inputReader.read()) {
                    case 't':
                        builder.append('\t');
                        break;

                    case 'r':
                        builder.append('\r');
                        break;

                    case 'n':
                        builder.append('\n');
                        break;

                    case 'f':
                        builder.append('\f');
                        break;

                    case 'b':
                        builder.append('\b');
                        break;

                    case '\"':
                        builder.append('\"');
                        break;

                    case '/':
                        builder.append('/');
                        break;

                    case '\\':
                        builder.append('\\');
                        break;

                    case 'u':
                        String escapedChar = Character.toString((char) inputReader.read())
                                + Character.toString((char) inputReader.read())
                                + Character.toString((char) inputReader.read())
                                + Character.toString((char) inputReader.read());

                        LOG.debug("string() found escaped \\u{}", escapedChar);

                        builder.append(Character.toString((char) Integer.parseInt(escapedChar, 16)));
                        break;

                    default:
                        throw new JSONFormatException();
                }
            } else if ((char) next <= '\u001F') {
                throw new JSONFormatException();
            } else {
                builder.append((char) next);
            }
        }

        return new JSONString(builder.toString());
    }

    private JSONNumber number(Reader inputReader) throws IOException {
        int sign = 1;
        StringBuilder numberStringBuilder = new StringBuilder();

        char firstChar = (char) inputReader.read();
        if (firstChar == '-') {
            sign = -2;

            numberStringBuilder.append(firstChar);

            firstChar = (char) inputReader.read();
            if (firstChar == '0') {
                sign = -1;
            }
        } else if (firstChar == '0') {
            sign = 0;
        }
        numberStringBuilder.append(firstChar);

        char next;

        if (sign == 1 || sign == -2) {
            do {
                inputReader.mark(1);
                next = (char) inputReader.read();

                if (Character.isDigit(next)) {
                    numberStringBuilder.append(next);
                } else {
                    inputReader.reset();
                    break;
                }
            } while (true);
        }

        inputReader.mark(1);
        next = (char) inputReader.read();
        if (next == '.') {
            numberStringBuilder.append(next);

            boolean first = true;

            do {
                inputReader.mark(1);
                next = (char) inputReader.read();

                if (Character.isDigit(next)) {
                    numberStringBuilder.append(next);
                } else if (first) {
                    throw new JSONFormatException();
                } else {
                    inputReader.reset();
                    break;
                }

                first = false;
            } while (true);
        } else {
            inputReader.reset();
        }

        inputReader.mark(1);
        next = (char) inputReader.read();
        if (next == 'e' || next == 'E') {
            numberStringBuilder.append(next);

            boolean first = true;

            next = (char) inputReader.read();
            if (next == '-' || next == '+') {
                numberStringBuilder.append(next);
            } else if (Character.isDigit(next)) {
                numberStringBuilder.append(next);

                first = false;
            } else {
                throw new JSONFormatException();
            }

            do {
                inputReader.mark(1);
                next = (char) inputReader.read();

                if (Character.isDigit(next)) {
                    numberStringBuilder.append(next);
                } else if (first) {
                    throw new JSONFormatException();
                } else {
                    inputReader.reset();
                    break;
                }

                first = false;
            } while (true);
        } else {
            inputReader.reset();
        }

        LOG.debug("number() found {}", numberStringBuilder);

        return new JSONNumber(numberStringBuilder.toString());
    }

    private JSONBoolean bool(Reader inputReader) throws IOException {
        switch (inputReader.read()) {
            case 't':
                if (inputReader.read() == 'r'
                        && inputReader.read() == 'u'
                        && inputReader.read() == 'e') {
                    return JSONBoolean.TRUE;
                }
                break;

            case 'f':
                if (inputReader.read() == 'a'
                        && inputReader.read() == 'l'
                        && inputReader.read() == 's'
                        && inputReader.read() == 'e') {
                    return JSONBoolean.FALSE;
                }
                break;
        }

        throw new JSONFormatException();
    }

    private JSONValue nul(Reader inputReader) throws IOException {
        if (inputReader.read() == 'n'
                && inputReader.read() == 'u'
                && inputReader.read() == 'l'
                && inputReader.read() == 'l') {
            return null;
        }

        throw new JSONFormatException();
    }

    private void whitespace(Reader inputReader) throws IOException {
        char next;
        do {
            inputReader.mark(1);
            next = (char) inputReader.read();
        } while (WHITESPACE_CHARACTERS.contains(next));

        inputReader.reset();
    }

    private JSONValue delegate(Reader inputReader) throws JSONFormatException, IOException {
        this.whitespace(inputReader);

        inputReader.mark(1);
        char firstChar = (char) inputReader.read();
        inputReader.reset();

        switch (firstChar) {
            case JSONFactory.OBJECT_START:
                return this.object(inputReader);

            case JSONFactory.ARRAY_START:
                return this.array(inputReader);

            case JSONFactory.STRING_SURROUND:
                return this.string(inputReader);

            case 't':
            case 'f':
                return this.bool(inputReader);

            case 'n':
                return this.nul(inputReader);

            default:
                if (firstChar == '-' || Character.isDigit(firstChar)) {
                    return this.number(inputReader);
                }

                throw new JSONFormatException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JSONValue parse(InputStream input) throws JSONFormatException, IOException {
        return this.parse(new InputStreamReader(input, StandardCharsets.UTF_8));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JSONValue parse(String input) throws JSONFormatException {
        try {
            return this.parse(new StringReader(input));
        } catch (IOException ex) {
            throw new JSONFormatException(ex);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JSONValue parse(Reader input) throws JSONFormatException, IOException {
        BufferedReader inputReader = new BufferedReader(input);

        JSONValue value = this.delegate(inputReader);

        this.whitespace(inputReader);

        if (inputReader.read() == -1) {
            inputReader.close();
            return value;
        } else {
            throw new JSONFormatException();
        }
    }

}
