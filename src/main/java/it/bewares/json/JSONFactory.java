/**
 *
 * Copyright 2017-2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json;

import it.bewares.json.io.exception.JSONFormatException;
import it.bewares.json.io.generator.JSONGenerator;
import it.bewares.json.io.parser.JSONParser;
import it.bewares.json.query.JSONQuery;
import it.bewares.json.tree.JSONArray;
import it.bewares.json.tree.JSONBoolean;
import it.bewares.json.tree.JSONNumber;
import it.bewares.json.tree.JSONObject;
import it.bewares.json.tree.JSONString;
import it.bewares.json.tree.JSONStructure;
import it.bewares.json.tree.JSONValue;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import lombok.Getter;
import lombok.Setter;

/**
 * The {@code JSONFactory} shall connect all functionalities presented by this
 * library, {@literal e.g.} to parse, to generate JSON and to query in
 * {@code JSONValue}s.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
@Singleton
@Named("JSON")
public final class JSONFactory {

    /**
     * This enum lists all possible JSON data types.
     */
    public static enum Type {

        OBJECT(JSONObject.class),
        ARRAY(JSONArray.class),
        STRING(JSONString.class),
        NUMBER(JSONNumber.class),
        BOOLEAN(JSONBoolean.class),
        NULL(null);

        @Getter
        private Class<? extends JSONValue> type;

        @Getter
        private boolean structure;

        Type(Class<? extends JSONValue> type) {
            this.type = type;
            this.structure = type != null && JSONStructure.class.isAssignableFrom(type);
        }

        /**
         * Creates a new instance of the data type.
         *
         * @return a newly allocated instance of the represented data type
         * @throws InstantiationException e.g. if the types {@code Class} is
         * abstract or an interface
         * @throws IllegalAccessException if the classes nullary constructor is
         * not accessible
         */
        public JSONValue createInstance() throws InstantiationException, IllegalAccessException {
            if (type == null) {
                return null;
            }
            return type.newInstance();
        }
    }

    public static final char OBJECT_START = '{';
    public static final char OBJECT_END = '}';
    public static final char ARRAY_START = '[';
    public static final char ARRAY_END = ']';
    public static final char SEPARATOR = ',';
    public static final char KEY_VALUE_SEPARATOR = ':';
    public static final char STRING_SURROUND = '"';
    public static final char STRING_ESCAPE = '\\';

    @Getter
    @Setter
    @Inject
    private JSONParser parser;

    @Getter
    @Setter
    @Inject
    private JSONGenerator generator;

    /**
     * Allocates a {@code JSONFactory} without a parser and a generator.
     *
     * This may be used in combination of a Dependency Injector.
     */
    public JSONFactory() {
    }

    /**
     * Allocates a {@code JSONFactory} with a parser and generator of the
     * specified types.
     *
     * @param parserType the type for the parser which shall be used
     * @param generatorType the type for the generator which shall be used
     */
    public JSONFactory(Class<? extends JSONParser> parserType, Class<? extends JSONGenerator> generatorType) {
        try {
            this.parser = parserType.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
        }

        try {
            this.generator = generatorType.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
        }
    }

    /**
     * Creates a JSON representative of the given Type of the given Object in
     * the {@code value} argument.
     *
     * @param <T> JSONValue Class of which the representative shall be an
     * instance
     * @param value Object to be represented by the return value
     * @return JSON representative of the given Object
     * @throws ClassCastException if the representative cannot be of the
     * demanded type
     */
    public <T extends JSONValue> T createValue(Object value) {
        Throwable cause = null;
        JSONValue representative = null;

        if (value == null) {
            return null;
        }

        if (value instanceof JSONValue) {
            if (value instanceof JSONObject) {
                ((JSONObject) value).setFactory(this);
            }

            if (value instanceof JSONArray) {
                ((JSONArray) value).setFactory(this);
            }

            representative = (JSONValue) value;
        }

        if (value instanceof Jsonable) {
            representative = ((Jsonable) value).json();
        }

        if (value instanceof Map) {
            JSONObject json = new JSONObject();

            json.setFactory(this);
            json.putAll((Map) value);

            representative = json;
        }

        if (value instanceof List || value instanceof Object[] || value.getClass().isArray()) {
            JSONArray json = new JSONArray();

            json.setFactory(this);
            if (value instanceof List) {
                json.addAll((List) value);
            } else {
                json.addAll(Arrays.asList((Object[]) value));
            }

            representative = json;
        }

        if (value instanceof CharSequence) {
            representative = new JSONString((CharSequence) value);
        }

        if (value instanceof Boolean) {
            representative = new JSONBoolean((Boolean) value);
        }

        if (value instanceof Number) {
            representative = new JSONNumber((Number) value);
        }

        if (representative != null) {
            try {
                return (T) representative;
            } catch (ClassCastException ex) {
                cause = ex;
            }
        }

        throw new IllegalArgumentException("cannot cast " + value.getClass().getName() + " to JSON pendant", cause);
    }

    /**
     * Creates the String which is valid JSON corresponding to the given
     * {@code JSONValue}.
     *
     * @param value JSON entry for which a JSON String shall be created
     * @return the representatives valid JSON in a String
     */
    public String generate(JSONValue value) {
        return this.generator.generate(value);
    }

    /**
     * Writes the valid JSON corresponding to the given {@code JSONValue} to the
     * specified {@code Writer}.
     *
     * @param value JSON entry for which a JSON String shall be created
     * @param writer
     * @throws IOException
     */
    public void generate(JSONValue value, Writer writer) throws IOException {
        this.generator.write(value, writer);
    }

    /**
     * Parses the given input and returns the {@code JSONValue} representative.
     *
     * @param input String to be parsed
     * @return {@code JSONValue} representative of the input
     * @throws JSONFormatException if the input is not valid JSON
     */
    public JSONValue parse(String input) throws JSONFormatException {
        return this.parser.parse(new String(input.substring(0)));
    }

    /**
     * Determines a {@code JSONValue} at which the {@code JSONQuery} points at.
     *
     * @param query a {@code JSONQuery} which shall be resolved
     * @param input value on which the query shall be resolved
     * @return an arbitrary {@code JSONValue} into which is resolved
     */
    public JSONValue find(JSONQuery query, JSONValue input) {
        return query.find(input);
    }

    /**
     * Determines all {@code JSONValue}s at which the {@code JSONQuery} points
     * at.
     *
     * @param query a {@code JSONQuery} which shall be resolved
     * @param input value on which the query shall be resolved
     * @return the {@code JSONValue}s into which are resolved
     */
    public Collection<JSONValue> resolve(JSONQuery query, JSONValue input) {
        return query.resolve(input);
    }

}
