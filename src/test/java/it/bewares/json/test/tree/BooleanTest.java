/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.test.tree;

import it.bewares.json.JSONFactory;
import it.bewares.json.tree.JSONBoolean;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * This test class is to test the JSONBoolean.
 *
 * @author Markus Goellnitz
 */
public class BooleanTest {

    @Test
    public void instanciation() {
        JSONBoolean bool = new JSONBoolean();
        bool.setValue(true);
        Assert.assertEquals(bool, JSONBoolean.TRUE);
        Assert.assertEquals(new JSONBoolean(), JSONBoolean.FALSE);
        Assert.assertEquals(new JSONBoolean(""), JSONBoolean.FALSE);
        Assert.assertEquals(new JSONBoolean("YES"), JSONBoolean.FALSE);
        Assert.assertEquals(new JSONBoolean("TRUE"), JSONBoolean.TRUE);
        Assert.assertEquals(new JSONBoolean("true"), JSONBoolean.TRUE);
        Assert.assertEquals(new JSONBoolean("tRUe"), JSONBoolean.TRUE);
    }

    @Test
    public void booleanSpecific() {
        JSONBoolean bool = new JSONBoolean();

        Assert.assertEquals((Boolean) bool.booleanValue(), Boolean.FALSE);
    }

    @Test
    public void valueSpecific() throws CloneNotSupportedException {
        JSONBoolean bool = new JSONBoolean();

        Assert.assertEquals(bool.getValue(), Boolean.FALSE);

        Assert.assertEquals(bool.type(), JSONFactory.Type.BOOLEAN);

        Assert.assertEquals(bool.clone(), bool);

        Assert.assertTrue(JSONBoolean.FALSE.compareTo(JSONBoolean.TRUE) < 0);
        Assert.assertTrue(JSONBoolean.FALSE.compareTo(JSONBoolean.FALSE) == 0);
        Assert.assertTrue(JSONBoolean.TRUE.compareTo(JSONBoolean.TRUE) == 0);
        Assert.assertTrue(JSONBoolean.TRUE.compareTo(JSONBoolean.FALSE) > 0);
    }

    @Test
    public void objectSpecific() {
        Assert.assertEquals(JSONBoolean.TRUE.toString(), "true");
        Assert.assertNotEquals(JSONBoolean.TRUE.toString(), "false");
        Assert.assertNotEquals(JSONBoolean.TRUE.toString(), "TRUE");
        Assert.assertEquals(JSONBoolean.FALSE.toString(), "false");
        Assert.assertNotEquals(JSONBoolean.FALSE.toString(), "true");
        Assert.assertNotEquals(JSONBoolean.FALSE.toString(), "FALSE");

        Assert.assertEquals(JSONBoolean.TRUE.hashCode(), 1372);
        Assert.assertEquals(JSONBoolean.FALSE.hashCode(), 1378);

        Assert.assertFalse(JSONBoolean.FALSE.equals(JSONBoolean.TRUE));
        Assert.assertTrue(JSONBoolean.FALSE.equals(JSONBoolean.FALSE));
        Assert.assertFalse(JSONBoolean.FALSE.equals(false));
        Assert.assertFalse(JSONBoolean.FALSE.equals(""));
    }

}
