/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.test.tree;

import it.bewares.json.JSONFactory;
import it.bewares.json.tree.JSONArray;
import it.bewares.json.tree.JSONBoolean;
import it.bewares.json.tree.JSONNumber;
import it.bewares.json.tree.JSONObject;
import java.util.Arrays;
import it.bewares.json.tree.JSONString;
import it.bewares.json.tree.JSONValue;
import java.math.BigInteger;
import java.util.HashMap;
import org.testng.Assert;
import org.testng.Assert.ThrowingRunnable;
import org.testng.annotations.Test;

/**
 * This test class is to test the JSONArray.
 *
 * @author Markus Goellnitz
 */
public class ArrayTest {

    JSONValue[] aVal = new JSONValue[]{null, new JSONString(""), null, new JSONNumber(3), JSONBoolean.TRUE, new JSONObject() {
        {
            put("a", "lol");
            put("null", 35);
        }
    }, new JSONArray(null, new JSONNumber(9.7))};
    JSONArray a = new JSONArray(aVal);

    @Test
    public void instanciation() {
        Assert.assertEquals(new JSONArray().size(), 0);

        Assert.assertEquals(a.iterator(), Arrays.asList(aVal).iterator());
        Assert.assertEquals(a.iterator(), new JSONArray(Arrays.asList(aVal)).iterator());
    }

    @Test
    public void listSpecific() {
        JSONArray b = new JSONArray();

        Assert.assertTrue(b.add(Boolean.TRUE));
        b.add(0, Boolean.TRUE);

        Assert.assertTrue(b.add(JSONBoolean.FALSE));
        b.add(2, JSONBoolean.FALSE);

        Assert.assertTrue(b.add(Arrays.asList(null, true)));
        b.add(4, Arrays.asList(null, true));

        Assert.assertTrue(b.add(new HashMap() {
            {
                put("a", "lol");
                put(null, 35);
            }
        }));
        b.add(6, new HashMap() {
            {
                put("a", "lol");
                put(null, 35);
            }
        });

        Assert.assertTrue(b.add(new BigInteger("34666")));
        b.add(8, new BigInteger("34666"));

        Assert.assertTrue(b.add("hi!"));
        b.add(10, "hi!");

        Assert.assertTrue(b.addAll(Arrays.asList(aVal)));
        Assert.assertTrue(b.addAll(12, Arrays.asList(aVal)));

        Assert.assertTrue(b.contains("hi!"));
        Assert.assertFalse(b.contains("hey!"));

        Assert.assertTrue(b.containsAll(Arrays.asList("hi!", "hi!", Boolean.FALSE, JSONBoolean.TRUE)));
        Assert.assertTrue(b.containsAll(Arrays.asList(34666)));

        Assert.assertEquals(b.get(9), new JSONNumber("34666"));

        Assert.assertFalse(b.isEmpty());

        Assert.assertEquals(b.indexOf(new BigInteger("34666")), 8);
        Assert.assertEquals(b.lastIndexOf(new BigInteger("34666")), 9);

        b.iterator();
        b.listIterator();
        b.listIterator(3);

        Assert.assertTrue(b.remove("hi!"));
        Assert.assertTrue(b.contains("hi!"));
        Assert.assertTrue(b.remove("hi!"));
        Assert.assertFalse(b.contains("hi!"));
        Assert.assertEquals(b.remove(3), JSONBoolean.FALSE);

        Object[] o = new Object[]{new BigInteger("34666"), Arrays.asList(null, true)};
        Object[] oVal = new Object[]{new JSONArray(null, JSONBoolean.TRUE), new JSONNumber("34666")};
        Object[] oVal1 = new Object[]{new JSONArray(null, JSONBoolean.TRUE), new JSONArray(null, JSONBoolean.TRUE),
            new JSONNumber("34666"), new JSONNumber("34666")};

        Assert.assertTrue(b.removeAll(Arrays.asList(Boolean.FALSE, Boolean.TRUE)));
        b.retainAll(Arrays.asList(o));

        Assert.assertEquals(b.size(), 4);

        Assert.assertEquals(b.subList(1, 3), Arrays.asList(oVal));
        Assert.assertEquals(b.toArray(), oVal1);
        Assert.assertEquals(b.toArray(new JSONValue[0]), oVal1);

        Assert.assertEquals(b.set(0, Boolean.TRUE), oVal1[0]);
        Assert.assertEquals(b.set(1, JSONBoolean.FALSE), oVal1[1]);
        Assert.assertEquals(b.set(2, Arrays.asList(null, true)), oVal1[2]);
        Assert.assertEquals(b.set(3, new HashMap() {
            {
                put("a", "lol");
                put(null, 35);
            }
        }), oVal1[3]);
        Assert.assertThrows(java.lang.IndexOutOfBoundsException.class, new ThrowingRunnable() {
            @Override
            public void run() throws Throwable {
                b.set(4, new BigInteger("34666"));
            }
        });

        b.set(1, new BigInteger("34666"));
        b.set(2, "hi!");

        Assert.assertEquals(b.size(), 4);

        b.clear();
        Assert.assertTrue(b.isEmpty());
    }

    @Test
    public void valueSpecific() throws CloneNotSupportedException {
        Assert.assertEquals(a.type(), JSONFactory.Type.ARRAY);

        Assert.assertEquals(a.clone(), a);
    }

    @Test
    public void objectMethods() {
        Assert.assertEquals(a.hashCode(), 1221089307);

        Assert.assertTrue(a.equals(new JSONArray(aVal)));

        Assert.assertEquals(a.toString(), "JSONArray[null, \"...\", null, 3, true, {...}, [...]]");
    }

}
