/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.test.tree;

import it.bewares.json.JSONFactory;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import it.bewares.json.tree.JSONNumber;
import org.testng.Assert;
import org.testng.Assert.ThrowingRunnable;
import org.testng.annotations.Test;

/**
 * This test class is to test the JSONNumber.
 *
 * @author Markus Goellnitz
 */
public class NumberTest {

    @Test
    public void instanciation() {
        Assert.assertEquals((new JSONNumber()).getValue(), BigDecimal.ZERO);
        Assert.assertEquals((new JSONNumber(10d)).getValue(), BigDecimal.TEN);
        Assert.assertEquals((new JSONNumber(1f)).getValue(), BigDecimal.ONE);
        Assert.assertEquals((new JSONNumber(1)).getValue(), BigDecimal.ONE);
        Assert.assertEquals((new JSONNumber(1000l)).getValue(), new BigDecimal(1000l));
        Assert.assertEquals((new JSONNumber(BigInteger.TEN)).getValue(), BigDecimal.TEN);
        Assert.assertEquals((new JSONNumber(new JSONNumber())).getValue(), BigDecimal.ZERO);

        for (String number : Arrays.asList(null, "hi", "{\"number\":5e3}")) {
            Assert.assertThrows(new ThrowingRunnable() {

                @Override
                public void run() throws Throwable {
                    new JSONNumber(number);
                }
            });
        }

        Assert.assertThrows(IllegalArgumentException.class, new ThrowingRunnable() {

            @Override
            public void run() throws Throwable {
                new JSONNumber((Number) null);
            }
        });
    }

    @Test
    public void numberSpecific() {
        JSONNumber number = new JSONNumber("1234567893e-5");
        JSONNumber numberB = new JSONNumber("1234567893e-6");

        Assert.assertEquals(number.intValue(), 12345);
        Assert.assertEquals(number.longValue(), 12345L);
        Assert.assertEquals(number.floatValue(), 12345.67893f);
        Assert.assertEquals(number.doubleValue(), 12345.67893);
        Assert.assertNotEquals(number.byteValue(), 12345);
        Assert.assertEquals(number.byteValue(), 57);
        Assert.assertEquals(number.shortValue(), (short) 12345);

        Assert.assertFalse(number.greaterThan(number));
        Assert.assertTrue(number.greaterThanOrEquals(number));
        Assert.assertFalse(number.lessThan(number));
        Assert.assertTrue(number.lessThanOrEquals(number));
        Assert.assertTrue(number.greaterThan(numberB));
        Assert.assertFalse(numberB.greaterThan(number));
        Assert.assertTrue(number.greaterThanOrEquals(numberB));
        Assert.assertFalse(numberB.greaterThanOrEquals(number));
        Assert.assertFalse(number.lessThan(numberB));
        Assert.assertTrue(numberB.lessThan(number));
        Assert.assertFalse(number.lessThanOrEquals(numberB));
    }

    @Test
    public void valueSpecific() throws CloneNotSupportedException {
        JSONNumber number = new JSONNumber();
        number.parseAndSetValue("1234567893e-5");

        Assert.assertEquals(number.getValue(), new BigDecimal("1234567893e-5"));

        Assert.assertEquals(number.type(), JSONFactory.Type.NUMBER);

        Assert.assertEquals(number.clone(), number);

        Assert.assertEquals(number.compareTo(number), 0);
    }

    @Test
    public void objectSpecific() {
        JSONNumber number = new JSONNumber("1234567893e-5");
        Assert.assertEquals(number.hashCode(), -383100647);

        Assert.assertFalse((new JSONNumber()).equals(number));
        Assert.assertFalse(number.equals("1234567893e-5"));
        Assert.assertTrue((new JSONNumber(number.toString())).equals(number));
    }

}
