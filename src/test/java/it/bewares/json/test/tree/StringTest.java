/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.test.tree;

import it.bewares.json.JSONFactory;
import java.util.Arrays;
import it.bewares.json.tree.JSONString;
import org.testng.Assert;
import org.testng.Assert.ThrowingRunnable;
import org.testng.annotations.Test;

/**
 * This test class is to test the JSONString.
 *
 * @author Markus Goellnitz
 */
public class StringTest {

    @Test
    public void instanciation() {
        Assert.assertEquals(new JSONString(), new JSONString(""));
        Assert.assertEquals(new JSONString("\"\"".subSequence(0, 2)), new JSONString("\"\""));
    }

    @Test
    public void charSpecific() {
        JSONString jsonString = new JSONString();
        String string = "";
        jsonString.setValue(string);

        Assert.assertEquals(jsonString.length(), string.length());

        string = "hi!";
        jsonString.setValue(string);

        Assert.assertEquals(jsonString.length(), string.length());

        Assert.assertEquals(jsonString.charAt(1), string.charAt(1));

        for (int i : Arrays.asList(-1, jsonString.length() + 1)) {
            Assert.assertThrows(IndexOutOfBoundsException.class, new ThrowingRunnable() {

                @Override
                public void run() throws Throwable {
                    jsonString.charAt(i);
                }
            });
        }

        Assert.assertEquals(jsonString.subSequence(1, jsonString.length() - 1), string.subSequence(1, string.length() - 1));

        string = "This is indeed awesomeness!";
        jsonString.setValue(string);

        Assert.assertEquals(jsonString.subSequence(2, 9), string.subSequence(2, 9));

        Assert.assertEquals(jsonString.indexOf("i"), 2);
        Assert.assertEquals(jsonString.indexOf("v"), -1);
        Assert.assertEquals(jsonString.indexOf('i'), 2);
        Assert.assertEquals(jsonString.indexOf("i", 4), 5);
        Assert.assertEquals(jsonString.indexOf('i', 4), 5);
        Assert.assertEquals(jsonString.indexOf("ee"), 11);

        Assert.assertEquals(jsonString.lastIndexOf("ee"), 11);
        Assert.assertEquals(jsonString.lastIndexOf("i"), 8);
        Assert.assertEquals(jsonString.lastIndexOf('i'), 8);
        Assert.assertEquals(jsonString.lastIndexOf("i", 6), 5);
        Assert.assertEquals(jsonString.lastIndexOf('i', 6), 5);
        Assert.assertEquals(jsonString.lastIndexOf("i", 10), 8);
        Assert.assertEquals(jsonString.lastIndexOf('i', 10), 8);
        Assert.assertEquals(jsonString.lastIndexOf("i", 1), -1);
        Assert.assertEquals(jsonString.lastIndexOf('i', 1), -1);
    }

    @Test
    public void stringSpecific() {
        Assert.assertEquals(JSONString.unescape("\"a\""), "a");
        Assert.assertEquals(JSONString.unescape("\"\\na\\\"\\u001a\""), "\na\"\u001a");

        for (String string : Arrays.asList("\"", "a\"", "\"a")) {
            Assert.assertThrows(IllegalArgumentException.class, new ThrowingRunnable() {

                @Override
                public void run() throws Throwable {
                    JSONString.unescape(string);
                }
            });
        }

        JSONString jsonString = new JSONString();
        jsonString.setValue(JSONString.unescape("\"a\""));

        Assert.assertEquals(jsonString, new JSONString("a"));
        Assert.assertTrue(jsonString.equalsIgnoreCase(new JSONString("A")));
    }

    @Test
    public void valueSpecific() throws CloneNotSupportedException {
        JSONString string = new JSONString();
        String testString = "arbitrary";
        string.setValue(testString);
        JSONString jsonString = new JSONString("a");

        Assert.assertEquals(string.getValue(), testString);

        Assert.assertEquals(string.type(), JSONFactory.Type.STRING);

        Assert.assertEquals(string.clone(), string);

        Assert.assertTrue(jsonString.compareTo(jsonString) == 0);
        Assert.assertTrue(jsonString.compareTo(new JSONString("A")) > 0);
        Assert.assertTrue(jsonString.compareTo(new JSONString("0")) > 0);
    }

    @Test
    public void objectMethods() {
        JSONString string = new JSONString("another arbitrary json string");
        JSONString jsonString = new JSONString("a");

        Assert.assertEquals(string.hashCode(), 512214187);

        Assert.assertFalse(jsonString.equals("a"));
        Assert.assertTrue(jsonString.equals(new JSONString("a")));
        Assert.assertFalse(jsonString.equals(new JSONString("A")));

        Assert.assertEquals(string.toString(), string.getValue());
    }

}
