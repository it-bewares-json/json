/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.test.query;

import java.util.Arrays;
import it.bewares.json.io.parser.JSONStreamParser;
import it.bewares.json.query.exception.JSONQueryFormatException;
import it.bewares.json.query.exception.JSONQueryResolveException;
import it.bewares.json.query.JSONSelector;
import it.bewares.json.query.condition.BasicCondition;
import it.bewares.json.tree.JSONNumber;
import it.bewares.json.tree.JSONString;
import it.bewares.json.tree.JSONValue;
import org.testng.Assert;
import org.testng.Assert.ThrowingRunnable;
import org.testng.annotations.Test;

/**
 * This test class is to test the JSONSelector.
 *
 * @author Markus Goellnitz
 */
public class SelectorTest {

    JSONValue testVal = new JSONStreamParser().parse("{\"planets\":[{\"name\":\"Earth\",\"technologicallyAdvancedSpecies\":\"homo sapiens\",\"hasBuiltSpaceships\":true,\"moons\":1},{\"name\":\"Kerbin\",\"technologicallyAdvancedSpecies\":\"Kerbal\",\"hasBuiltSpaceships\":true,\"moons\":2}],\"stars\":[]}");

    @Test
    public void selectorSpecific() {
        Assert.assertEquals(new JSONSelector().find(testVal), new JSONSelector("").find(testVal));
        Assert.assertEquals(new JSONSelector(".\"planets\".0").getSelector(), ".\"planets\".0");
        Assert.assertEquals(new JSONSelector(".\"planets\".(@.\"name\"==\"Earth\").\"moons\"").getConditions(),
                Arrays.asList(null, new BasicCondition("@.\"name\"", BasicCondition.Comparator.EQUAL, new JSONString("Earth")), null));
    }

    @Test
    public void invalidFormats() {
        for (String selector : Arrays.asList(".",
                ".()",
                ".(@key ==)",
                ".(@key == \"planets)",
                ".(@kye == \"planets\")",
                ".(#.\"planets\".0.\"name == \"Earth\")",
                ".(@key != nell)",
                ".(nell == @key)",
                ".(key == \"planets\")",
                ".\"planets\".(true == true & false != true)",
                ".\"planets\".(@.\"name\" ~ \"^Ea\" & @.\"name\" != \"Kerbin\")")) {
            Assert.assertThrows(JSONQueryFormatException.class, new ThrowingRunnable() {

                @Override
                public void run() throws Throwable {
                    new JSONSelector(selector);
                    throw new Exception("did accept invalid selector: " + selector);
                }
            });
        }
    }

    @Test
    public void invalidQueries() {
        for (String selector : Arrays.asList(".1",
                ".\"planets\".0.\"moons\".3",
                ".\"planets\".(@key == \"(believe me!)\")")) {
            Assert.assertThrows(JSONQueryResolveException.class, new ThrowingRunnable() {

                @Override
                public void run() throws Throwable {
                    JSONValue result = new JSONSelector(selector).find(testVal);
                    throw new Exception("did accept selector: " + selector + " and found " + result);
                }
            });
        }
    }

    @Test
    public void definiteQueries() {
        Assert.assertEquals(new JSONSelector("").find(testVal), testVal);
        Assert.assertEquals(new JSONSelector(".\"planets\".0.\"moons\"").find(testVal), new JSONNumber(1));
    }

    @Test
    public void indefiniteQueries() {
        for (String selector : Arrays.asList(".\"planets\".(\n @.\"name\" == \t \"Earth\" ).\"moons\"",
                ".\"planets\".(@.\"name\"==\"Earth\").\"moons\"",
                ".\"planets\".(@.\"name\" == \"Earth\").\"moons\"",
                ".\"planets\".(@.\"name\" == #.\"planets\".0.\"name\").\"moons\"",
                ".\"planets\".(@.\"name\" != \"Kerbin\").\"moons\"",
                ".(@key == \"planets\").0.\"moons\"",
                ".\"planets\".(@index < 1).\"moons\"",
                ".\"planets\".(@index <= 0).\"moons\"",
                ".\"planets\".(@index == 0).\"moons\"",
                ".\"planets\".(@.\"name\" ~ \"^E\").\"moons\"",
                ".\"planets\".(@.\"name\" ~ \"^V\" || @.\"name\" ~ \"^E\").\"moons\"",
                ".\"planets\".(@.\"name\" ~ \"^E\" || @.\"name\" ~ \"^V\").\"moons\"",
                ".\"planets\".(@.\"name\" == \"Earth\" && false != true).\"moons\"",
                ".\"planets\".(@.\"name\" == #.\"planets\".0.\"name\").\"moons\"")) {
            Assert.assertEquals(new JSONSelector(selector).find(testVal), new JSONNumber(1));
        }

        for (String selector : Arrays.asList(".\"planets\".(@index > 0).\"moons\"",
                ".\"planets\".(@index >= 1).\"moons\"")) {
            Assert.assertEquals(new JSONSelector(selector).find(testVal), new JSONNumber(2));
        }
    }

}
