/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.test.io.parser;

import java.util.Arrays;
import java.util.HashMap;
import it.bewares.json.io.parser.JSONParser;
import it.bewares.json.tree.JSONArray;
import it.bewares.json.tree.JSONBoolean;
import it.bewares.json.tree.JSONNumber;
import it.bewares.json.tree.JSONObject;
import it.bewares.json.tree.JSONString;
import it.bewares.json.io.exception.JSONFormatException;
import it.bewares.json.io.parser.JSONStreamParser;
import java.io.IOException;
import java.io.StringBufferInputStream;
import java.io.StringReader;
import java.lang.reflect.Method;
import org.testng.Assert;
import org.testng.ITest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

/**
 * This test class is to test a JSONParser implementation.
 *
 * @author Markus Goellnitz
 */
public class ParsingTest implements ITest {

    JSONParser parser;

    @Factory(dataProvider = "parsers")
    public ParsingTest(JSONParser parser) {
        this.parser = parser;
    }

    @DataProvider
    public static Object[][] parsers() {
        return new Object[][]{
            {new JSONStreamParser()}
        };
    }

    @Test
    public void objectParsing() {
        Assert.assertEquals(parser.parse("{}"), new JSONObject(), "parse an empty object");
        Assert.assertEquals(parser.parse("{\"this\":\"failed?\"}"), new JSONObject(new HashMap() {
            {
                put("this", "failed?");
            }
        }), "parse an object with a single pair");
        Assert.assertEquals(parser.parse("{\"this\":\"failed?\",\"so\":\"true\",\"except it!\":\"now, or hey wowo, dat is it! WHUT???? NOO!!!!! yes. :C but?\"}"), new JSONObject(new HashMap() {
            {
                put("this", "failed?");
                put("so", "true");
                put("except it!", "now, or hey wowo, dat is it! WHUT???? NOO!!!!! yes. :C but?");
            }
        }), "parse an object with multiple pairs");
    }

    @Test
    public void arrayParsing() {
        Assert.assertEquals(parser.parse("[]"), new JSONArray(), "parse an empty array");
        Assert.assertEquals(parser.parse("[true]"), new JSONArray(JSONBoolean.TRUE), "parse an array with a single entry");
        Assert.assertEquals(parser.parse("[\"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ\",false,true]"), new JSONArray(new JSONString("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), JSONBoolean.FALSE, JSONBoolean.TRUE), "parse an array with multiple entries");
    }

    @Test
    public void numberParsing() {
        Assert.assertEquals(parser.parse("0"), new JSONNumber("0"), "parse a number 0");
        Assert.assertEquals(parser.parse("1"), new JSONNumber("1"), "parse a number 1");
        Assert.assertEquals(parser.parse("-3"), new JSONNumber("-3"), "parse a number -3");
        Assert.assertEquals(parser.parse("-5.0e-3"), new JSONNumber("-0.005"), "parse a number -5e-3/-0.005");
        Assert.assertEquals(parser.parse("4e2"), new JSONNumber("400"), "parse a number 4e2/400");
        Assert.assertEquals(parser.parse("12e+3"), new JSONNumber("12000"));
        Assert.assertEquals(parser.parse("-0.375"), new JSONNumber("-0.375"));
        Assert.assertEquals(parser.parse("1E3"), new JSONNumber("1e3"));
    }

    @Test
    public void stringParsing() {
        Assert.assertEquals(parser.parse("\"\""), new JSONString(""), "parse an empty String \"\"");
        Assert.assertEquals(parser.parse("\"\\\"\""), new JSONString("\""), "parse a String \"\\\"\"");
        Assert.assertEquals(parser.parse("\"hey, whut's up?\""), new JSONString("hey, whut's up?"), "parse a String \"hey, whut's up?\"");
        Assert.assertEquals(parser.parse("\"line 1\\nline2\""), new JSONString("line 1\nline2"), "parse a String \"line 1\\nline2\"");
        Assert.assertEquals(parser.parse("\"line 1\\rline2\""), new JSONString("line 1\rline2"), "parse a String \"line 1\\rline2\"");
        Assert.assertEquals(parser.parse("\"a\\tb\""), new JSONString("a\tb"), "parse a String \"a\\tb\"");
        Assert.assertEquals(parser.parse("\"\\\" \\\\ \\/ \\n \\r \\t \\b \\f\""), new JSONString("\" \\ / \n \r \t \b \f"));
        Assert.assertEquals(parser.parse("\"\\\"\\\\\\/\\f\\b\\t\\r\\n\""), new JSONString("\"\\/\f\b\t\r\n"));
        Assert.assertEquals(parser.parse("\"\\u001a \\u000b \\u1234\""), new JSONString("\u001a \u000b \u1234"));
        Assert.assertEquals(parser.parse("\"\\uD834\\uDD1E\""), new JSONString("𝄞"));
    }

    @Test
    public void invalidParsing() {
        for (String json : Arrays.asList("",
                "{",
                "}",
                "]",
                "\"abc",
                "{\"this\": nu, \"ll\": null}",
                "{\"I\": \"am\": \"awesome\"}",
                "{\"I\", \"am awesome\"}",
                "{\"I\"}",
                "{\this is\": \"supposed to fail!\"}",
                "[true,false,null,0",
                "{input: null}",
                "{\"input\": \"hi}",
                "{\"input\": {}",
                "{\"input\": {\"hey\": null, \"it\": \"is\"}",
                "{input: [}",
                "[true,false,null,whut]",
                "[true,false,null",
                "whut",
                "{\"This\":[\"is not[ valid.\"]]}",
                "{\"This\",\"is not valid.\"}",
                "[\"This\":\"is not valid.\"]",
                "\"\u001F\"",
                "\"\u0009\"",
                "{\"a\": null} a",
                "01.3e4",
                "23.",
                "12.45e",
                "12.45E-",
                "\"\\\\\\n\\v\"",
                "ture",
                "treu",
                "truw",
                "flase",
                "fasle",
                "fales",
                "falsr",
                "nlul",
                "nukk",
                "nul",
                "\b")) {
            Assert.assertThrows(JSONFormatException.class, new Assert.ThrowingRunnable() {

                @Override
                public void run() throws Throwable {
                    parser.parse(json);
                    throw new Exception("did parse invalid json: " + json);
                }
            });
        }
    }

    @Test
    public void arbitraryParsing() {
        Assert.assertEquals(parser.parse("{\"this\":[]}"), new JSONObject(new HashMap() {
            {
                put("this", Arrays.asList());
            }
        }), "parse an object with empty an array");
        Assert.assertEquals(parser.parse("{\"this\":[\"failed!\", \"or\", \"did not\"]}"), new JSONObject(new HashMap() {
            {
                put("this", Arrays.asList("failed!", "or", "did not"));
            }
        }), "parse an object with a string-containing array");
        Assert.assertEquals(parser.parse("{\"this\":[\"failed?\", {\"was\": \"successful\"}]}"), new JSONObject(new HashMap() {
            {
                put("this", Arrays.asList("failed?", new HashMap() {
                    {
                        put("was", "successful");
                    }
                }));
            }
        }), "parse an object with an array containing an object");
        Assert.assertEquals(parser.parse("{\"this\":[\"failed?\", {\"with\": null}]}"), new JSONObject(new HashMap() {
            {
                put("this", Arrays.asList("failed?", new HashMap() {
                    {
                        put("with", null);
                    }
                }));
            }
        }), "parse an object with an array containing an object containing NULL");
        Assert.assertEquals(parser.parse("[[],{},true,1,2]"), new JSONArray(new JSONArray(), new JSONObject(), JSONBoolean.TRUE, new JSONNumber(1), new JSONNumber(2)), "parse a json array with some entries");
        Assert.assertEquals(parser.parse("[[[],{}],{},true,1,null,false,2]"), new JSONArray(new JSONArray(new JSONArray(new JSONArray(), new JSONObject())), new JSONObject(), JSONBoolean.TRUE, new JSONNumber(1), null, JSONBoolean.FALSE, new JSONNumber(2)), "parse an array with some entries");
    }

    @Test
    public void alternativeParseInputs() throws JSONFormatException, IOException {
        String s = "[[[],{}],{},true,1,null,false,2]";
        JSONArray v = new JSONArray(new JSONArray(new JSONArray(new JSONArray(), new JSONObject())), new JSONObject(), JSONBoolean.TRUE, new JSONNumber(1), null, JSONBoolean.FALSE, new JSONNumber(2));
        Assert.assertEquals(parser.parse(new StringBufferInputStream(s)), v);
        Assert.assertEquals(parser.parse(new StringReader(s)), v);
    }

    private String testName = "";

    @BeforeMethod(alwaysRun = true)
    public void setTestName(Method method) {
        this.testName = String.format("%s(%s)", method.getName(), this.parser.getClass().getSimpleName());
    }

    @Override
    public String getTestName() {
        return testName;
    }

}
